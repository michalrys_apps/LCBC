package com.michalrys.lcbc.loadcols;

public class LoadcolMoment extends Loadcol {
    public LoadcolMoment() {
    }

    public LoadcolMoment(String inputText) {
        setLoadType(LoadType.MOMENT);
        addToNastranTable(inputText);
        setFemId(Integer.parseInt(getNastranTable().get(2).replaceAll(" ", "")));
    }

    @Override
    public String toString() {
        return "LoadcolMoment{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}