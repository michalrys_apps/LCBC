package com.michalrys.lcbc.loadcols;

public enum LoadType {
    FORCE,
    SPC,
    SPCADD,
    GRAV,
    LOAD,
    PLOAD4,
    MOMENT,
    COLOR_ONLY,
    NAME_ONLY
}
