package com.michalrys.lcbc.loadcols;

public class LoadcolSpcAdd extends Loadcol {
    public LoadcolSpcAdd() {
    }

    public LoadcolSpcAdd(String inputText) {
        setLoadType(LoadType.SPCADD);
        addToNastranTable(inputText);
    }

    @Override
    public String toString() {
        return "LoadcolSpc{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}