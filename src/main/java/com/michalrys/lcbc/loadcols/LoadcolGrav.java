package com.michalrys.lcbc.loadcols;

import java.util.List;

public class LoadcolGrav extends Loadcol {
    public LoadcolGrav() {
    }

    public LoadcolGrav(String inputText) {
        setLoadType(LoadType.GRAV);
        addToNastranTable(inputText);
    }

    @Override
    public void addContinuationData(String readLine) {
        return;
    }

    @Override
    public String toString() {
        return "LoadcolGrav{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}