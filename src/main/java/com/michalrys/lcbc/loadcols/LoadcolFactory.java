package com.michalrys.lcbc.loadcols;

public class LoadcolFactory {
    public static final String $_HWCOLOR_LOADCOL = "$HWCOLOR LOADCOL";
    private static final String $_HMNAME_LOADCOL = "$HMNAME LOADCOL";
    public static final String GRAV = "GRAV";
    public static final String SPC = "SPC";
    public static final String SPCADD = "SPCADD";
    public static final String LOAD = "LOAD";
    public static final String PLOAD4 = "PLOAD4";
    public static final String FORCE = "FORCE";
    public static final String MOMENT = "MOMENT";

    public Loadcol make(String readLine) {
        if (readLine.contains($_HMNAME_LOADCOL)) {
            return new LoadcolNameOnly(readLine);
        }
        if (readLine.contains($_HWCOLOR_LOADCOL)) {
            return new LoadcolColorOnly(readLine);
        }

        String loadcolTypeInFirst8Characters = readLine.substring(0, 8);

        if (loadcolTypeInFirst8Characters.trim().equals(GRAV)) {
            return new LoadcolGrav(readLine);
        }
        if (loadcolTypeInFirst8Characters.trim().equals(SPC)) {
            return new LoadcolSpc(readLine);
        }
        if (loadcolTypeInFirst8Characters.trim().equals(SPCADD)) {
            return new LoadcolSpcAdd(readLine);
        }
        if (loadcolTypeInFirst8Characters.trim().equals(LOAD)) {
            return new LoadcolLoad(readLine);
        }
        if (loadcolTypeInFirst8Characters.trim().equals(PLOAD4)) {
            return new LoadcolPload4(readLine);
        }
        if (loadcolTypeInFirst8Characters.trim().equals(FORCE)) {
            return new LoadcolForce(readLine);
        }
        if (loadcolTypeInFirst8Characters.trim().equals(MOMENT)) {
            return new LoadcolMoment(readLine);
        }

        return null;
    }
}
