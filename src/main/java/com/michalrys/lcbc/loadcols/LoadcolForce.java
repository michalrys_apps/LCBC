package com.michalrys.lcbc.loadcols;

public class LoadcolForce extends Loadcol {

    public LoadcolForce() {
    }

    public LoadcolForce(String inputText) {
        setLoadType(LoadType.FORCE);
        addToNastranTable(inputText);
        setFemId(Integer.parseInt(getNastranTable().get(2).replaceAll(" ", "")));
    }

    @Override
    public String toString() {
        return "LoadcolForce{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}