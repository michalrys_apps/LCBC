package com.michalrys.lcbc.loadcols;

public class LoadcolSpc extends Loadcol {
    public LoadcolSpc() {
    }

    public LoadcolSpc(String inputText) {
        setLoadType(LoadType.SPC);
        addToNastranTable(inputText);
        setFemId(Integer.parseInt(getNastranTable().get(2).replaceAll(" ", "")));
    }

    @Override
    public void addContinuationData(String readLine) {
        return;
    }

    @Override
    public String toString() {
        return "LoadcolSpc{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}