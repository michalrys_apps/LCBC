package com.michalrys.lcbc.loadcols;

public class LoadcolNameOnly extends Loadcol {
    private static final String NASTRAN_EMPTY_COL = "        ";

    public LoadcolNameOnly(String inputText) {
        //$HMNAME LOADCOL              202"Pressure_air_1MPa"
        setId(Integer.parseInt(inputText.substring(15, 32).trim()));
        setFemId(null);
        setLoadType(LoadType.NAME_ONLY);
        setDescription(inputText.substring(32).replaceAll("\"", ""));
        addToNastranTableIn(0, NASTRAN_EMPTY_COL);

        int howManyEmptySpacesForId = 8 - getId().toString().length();
        addToNastranTableIn(1, emptySpaces(howManyEmptySpacesForId) + getId().toString());
    }

    @Override
    public void addContinuationData(String readLine) {
        return;
    }

    @Override
    public String toString() {
        return "LoadcolNameOnly{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}
