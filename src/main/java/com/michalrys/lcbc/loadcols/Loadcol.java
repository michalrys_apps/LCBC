package com.michalrys.lcbc.loadcols;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class Loadcol {
    private Integer id;
    private Integer femId;
    private Integer colorId;
    private String description;
    private LoadType loadType;
    private List<String> nastranTable = new ArrayList<>();
    private String bdfEntry;

    public void addContinuationData(String readLine){
        int endLoop = readLine.length() / 8;
        for (int i = 0; i < endLoop; i++) {
            int start = i * 8;
            int end = start + 8;
            getNastranTable().add(readLine.substring(start, end));
        }
    }

    void addToNastranTable(String inputText) {
        id = Integer.parseInt(inputText.substring(8, 16).trim());
        femId = null;

        int loadTypeHowManyEmptySpaces = 8 - loadType.toString().length();
        String loadTypeEmptySpaces = emptySpaces(loadTypeHowManyEmptySpaces);

        nastranTable.add(0, loadType.toString() + loadTypeEmptySpaces);

        int idHowManyEmptySpaces = 8 - id.toString().length();
        String idEmptySpaces = emptySpaces(idHowManyEmptySpaces);

        nastranTable.add(1, idEmptySpaces + id.toString());

        int endLoop = inputText.length() / 8;
        for (int i = 2; i < endLoop; i++) {
            int start = i * 8;
            int end = start + 8;

            nastranTable.add(i, inputText.substring(start, end));
        }
    }

    public void updateAllFieldsFrom(Loadcol loadcol) {
        if (loadcol.getId() != null) {
            id = loadcol.getId();
        }
        if (loadcol.getDescription() != null) {
            description = loadcol.getDescription();
        }
        if (loadcol.getLoadType() != null) {
            loadType = loadcol.getLoadType();
        }
        if (loadcol.getColorId() != null) {
            colorId = loadcol.getColorId();
        }
        if (loadcol.getLoadType() != null) {
            loadType = loadcol.getLoadType();
        }
        nastranTable = loadcol.getNastranTable();
    }

    String emptySpaces(int howMany) {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= howMany; i++) {
            result.append(" ");
        }
        return result.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFemId() {
        return femId;
    }

    public void setFemId(Integer femId) {
        this.femId = femId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LoadType getLoadType() {
        return loadType;
    }

    public void setLoadType(LoadType loadType) {
        this.loadType = loadType;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public List<String> getNastranTable() {
        return nastranTable;
    }

    public void setNastranTable(List<String> nastranTable) {
        this.nastranTable = nastranTable;
    }

    public void addToNastranTableIn(int location, String text) {
        nastranTable.add(location, text);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loadcol loadcol = (Loadcol) o;
        return Objects.equals(id, loadcol.id) &&
                Objects.equals(femId, loadcol.femId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, femId);
    }


    public void setBDFentry(String bdfEntry) {
        this.bdfEntry = bdfEntry;
    }

    public String getBdfEntry() {
        return bdfEntry;
    }
}
