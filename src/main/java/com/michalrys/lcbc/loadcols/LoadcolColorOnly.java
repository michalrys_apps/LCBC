package com.michalrys.lcbc.loadcols;

public class LoadcolColorOnly extends Loadcol {
    private static final String NASTRAN_EMPTY_COL = "        ";

    public LoadcolColorOnly(String inputText) {
        //$HWCOLOR LOADCOL             201      29
        setId(Integer.parseInt(inputText.substring(16, 32).trim()));
        setFemId(null);
        setLoadType(LoadType.COLOR_ONLY);
        setColorId(Integer.parseInt(inputText.substring(33).trim()));

        addToNastranTableIn(0, NASTRAN_EMPTY_COL);
        int howManyEmptySpacesForId = 8 - getId().toString().length();
        addToNastranTableIn(1, emptySpaces(howManyEmptySpacesForId) + getId().toString());
    }

    @Override
    public void addContinuationData(String readLine) {
        return;
    }

    @Override
    public String toString() {
        return "LoadcolColorOnly{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}