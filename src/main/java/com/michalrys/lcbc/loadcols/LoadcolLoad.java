package com.michalrys.lcbc.loadcols;

public class LoadcolLoad extends Loadcol {

    public LoadcolLoad() {
    }

    public LoadcolLoad(String inputText) {
        setLoadType(LoadType.LOAD);
        addToNastranTable(inputText);
    }

    // fixme I need also continuation data
//    @Override
//    public void addContinuationData(String readLine) {
//        return;
//    }

    @Override
    public String toString() {
        return "LoadcolLoad{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}