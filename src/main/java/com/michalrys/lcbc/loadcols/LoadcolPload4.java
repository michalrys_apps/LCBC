package com.michalrys.lcbc.loadcols;

public class LoadcolPload4 extends Loadcol {
    public LoadcolPload4() {
    }

    public LoadcolPload4(String inputText) {
        setLoadType(LoadType.PLOAD4);
        addToNastranTable(inputText);
        setFemId(Integer.parseInt(getNastranTable().get(2).replaceAll(" ", "")));
    }

    @Override
    public String toString() {
        return "LoadcolPload4{" +
                "id=" + getId() +
                ", femId=" + getFemId() +
                ", colorId=" + getColorId() +
                ", description='" + getDescription() + '\'' +
                ", loadType=" + getLoadType() +
                ", nastranTable=" + getNastranTable() +
                '}';
    }
}