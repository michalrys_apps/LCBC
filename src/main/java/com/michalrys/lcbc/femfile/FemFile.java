package com.michalrys.lcbc.femfile;

public interface FemFile {
    void readLoadcasesAndLoadCols();

    void updateLoadCols();

    void writeToFile();
}
