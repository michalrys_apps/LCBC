package com.michalrys.lcbc.femfile;


import com.michalrys.lcbc.AppRun;
import com.michalrys.lcbc.container.Container;
import com.michalrys.lcbc.container.LoadcaseContainer;
import com.michalrys.lcbc.container.LoadcolContainer;
import com.michalrys.lcbc.loadcase.Loadcase;
import com.michalrys.lcbc.loadcols.LoadType;
import com.michalrys.lcbc.loadcols.Loadcol;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class AnyFemFile implements FemFile {
    private static final String $HMNAME_LOADSTEP = "$HMNAME LOADSTEP";
    private static final String SUBCASE_ = "SUBCASE";
    private static final String __LABEL_ = "LABEL=";
    private static final String __SPC___ = "SPC =";
    private static final String __LOAD__ = "LOAD =";
    private static final String PLOAD_4__ = "PLOAD4  ";
    private static final String $_HMNAME_LOADCOL = "$HMNAME LOADCOL";
    private static final String $_HWCOLOR_LOADCOL = "$HWCOLOR LOADCOL";
    private static final String GRAV____ = "GRAV    ";
    private static final String SPCADD__ = "SPCADD  ";
    private static final String LOAD____ = "LOAD    ";
    private static final String SPC_____ = "SPC     ";
    private static final String PLUS_____ = "+       ";
    private static final String FORCE___ = "FORCE   ";
    private static final String MOMENT__ = "MOMENT  ";
    private static final String NASTRAN_EMPTY_COL = "        ";
    private final String filePath;
    private final String fileName;
    private final Container loadcols;
    private final LoadcaseContainer loadcases;

    public AnyFemFile(String filePath) {
        this.filePath = filePath;
        fileName = extractFileName(filePath);
        loadcols = new LoadcolContainer();
        loadcases = new LoadcaseContainer();
    }

    @Override
    public void readLoadcasesAndLoadCols() {
        System.out.println("File path: \"" + filePath + "\"");
        System.out.println("File name: \"" + fileName + "\"");
        AppRun.updateStatus("File path: \"" + filePath + "\"");
        AppRun.updateStatus("File name: \"" + fileName + "\"");

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String readLine;
            boolean shouldNotStartExtractingLoadcols = true;
            boolean shouldNotStartExtractingLoadcases = true;
            Loadcol currentLoadcol = null;
            Loadcase currentLoadcase = null;

            AppRun.updateStatus("LC/BC: parsing fem file...");
            while ((readLine = bufferedReader.readLine()) != null) {
                // -----------------------------------------------------------------------------------------------------
                // -------------- LOADSTEPS ----------------------------------------------------------------------------
                // -----------------------------------------------------------------------------------------------------
                if (readLine.contains("$HMNAME LOADCASES") || readLine.contains("SUBCASE")) {
                    shouldNotStartExtractingLoadcases = false;
                }
                if (cointainsUsefullDataForLoadcase(readLine)) {
                    currentLoadcase = loadcases.update(readLine);
                    continue;
                }
                if (cointainsContinuationOfDataForLoadcase(readLine)) {
                    loadcases.updateByAdditionalData(currentLoadcase, readLine);
                    continue;
                }
                if (readLine.contains("GRID")) {
                    shouldNotStartExtractingLoadcases = true;
                }
                // -----------------------------------------------------------------------------------------------------
                // -------------- LOADCOLS -----------------------------------------------------------------------------
                // -----------------------------------------------------------------------------------------------------
                if (readLine.contains("$HMNAME LOADCOL")) {
                    shouldNotStartExtractingLoadcols = false;
                }
                if (shouldNotStartExtractingLoadcases && shouldNotStartExtractingLoadcols) {
                    continue;
                }
                if (shouldNotStartExtractingLoadcols) {
                    continue;
                }
                if (cointainsUsefullData(readLine)) {
                    currentLoadcol = loadcols.update(readLine);
                    continue;
                }
                if (cointainsContinuationOfData(readLine)) {
                    loadcols.updateByAdditionalData(currentLoadcol, readLine);
                    continue;
                }
                if (cointainsContinuationOfDataForSpcAdd(readLine, currentLoadcol)) {
                    loadcols.updateByAdditionalData(currentLoadcol, readLine);
                }
            }
            AppRun.updateStatus("LC/BC: parsing fem file - done.");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        loadcases.printAll();
//        loadcols.printAll();
    }

    @Override
    public void updateLoadCols() {
        loadcols.updateDescriptionsAndColors();

        loadcases.printAll();
        loadcols.printAll();
    }

    private boolean cointainsUsefullDataForLoadcase(String lineOfText) {
        return (lineOfText.contains($HMNAME_LOADSTEP) ||
                lineOfText.contains(SUBCASE_));
    }

    private boolean cointainsContinuationOfDataForLoadcase(String lineOfText) {
        return (lineOfText.contains(__LABEL_) ||
                lineOfText.contains(__SPC___) ||
                lineOfText.contains(__LOAD__));
    }

    private boolean cointainsUsefullData(String lineOfText) {
        return (lineOfText.contains($_HWCOLOR_LOADCOL) ||
                lineOfText.contains($_HMNAME_LOADCOL) ||
                lineOfText.contains(GRAV____) ||
                lineOfText.contains(LOAD____) ||
                lineOfText.contains(PLOAD_4__) ||
                lineOfText.contains(FORCE___) ||
                lineOfText.contains(MOMENT__) ||
                lineOfText.contains(SPCADD__) ||
                lineOfText.contains(SPC_____));
    }

    private boolean cointainsContinuationOfData(String lineOfText) {
        return lineOfText.contains(PLUS_____);
    }

    private boolean cointainsContinuationOfDataForSpcAdd(String lineOfText, Loadcol currentLoadcol) {
        if (lineOfText.length() < 8) {
            return false;
        }
        return (lineOfText.substring(0, 8).equals(NASTRAN_EMPTY_COL)
                && (currentLoadcol.getLoadType().equals(LoadType.SPCADD)));
    }

    private String extractFileName(String filePath) {
        String fileNameWithExtension = filePath.replaceAll(".*/", "");
        return fileNameWithExtension.replaceAll("\\..*", "");
    }

    //TODO for test only
    public List<Loadcol> getListOfGrav() {
        return loadcols.getLoadcolGravs();
    }

    public List<Loadcol> getLoadcolSpcs() {
        return loadcols.getLoadcolSpcs();
    }

    public List<Loadcol> getLoadcolSpcAdds() {
        return loadcols.getLoadcolSpcAdds();
    }

    public List<Loadcol> getLoadcolLoads() {
        return loadcols.getLoadcolLoads();
    }

    public List<Loadcol> getLoadcolPload4s() {
        return loadcols.getLoadcolPload4s();
    }

    public List<Loadcol> getLoadcolForces() {
        return loadcols.getLoadcolForces();
    }

    public List<Loadcol> getLoadcolMoment() {
        return loadcols.getLoadcolMoment();
    }

    public List<Loadcase>  getLoadcases() {
        return loadcases.getList();
    }

    @Override
    public void writeToFile() {
    }
}