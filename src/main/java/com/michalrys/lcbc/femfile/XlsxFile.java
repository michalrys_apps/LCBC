package com.michalrys.lcbc.femfile;

import com.michalrys.lcbc.AppRun;
import com.michalrys.lcbc.container.Container;
import com.michalrys.lcbc.container.LoadcaseContainer;
import com.michalrys.lcbc.container.LoadcolContainer;
import com.michalrys.lcbc.loadcase.Loadcase;
import com.michalrys.lcbc.loadcols.*;
import com.michalrys.lcbc.tools.BDFprintTools;
import javafx.scene.control.Alert;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class XlsxFile implements FemFile {
    private static final String $HMNAME_LOADSTEP = "$HMNAME LOADSTEP";
    private static final String SUBCASE_ = "SUBCASE";
    private static final String __LABEL_ = "LABEL=";
    private static final String __SPC___ = "SPC =";
    private static final String __LOAD__ = "LOAD =";
    private static final String PLOAD_4__ = "PLOAD4  ";
    private static final String $_HMNAME_LOADCOL = "$HMNAME LOADCOL";
    private static final String $_HWCOLOR_LOADCOL = "$HWCOLOR LOADCOL";
    private static final String GRAV____ = "GRAV    ";
    private static final String SPCADD__ = "SPCADD  ";
    private static final String LOAD____ = "LOAD    ";
    private static final String SPC_____ = "SPC     ";
    private static final String PLUS_____ = "+       ";
    private static final String FORCE___ = "FORCE   ";
    private static final String MOMENT__ = "MOMENT  ";
    private static final String NASTRAN_EMPTY_COL = "        ";
    private final String filePath;
    private final String filePathForNewBDF;
    private final String fileName;
    private final Container loadcols;
    private final LoadcaseContainer loadcases;
    private List<Integer> listOfAllLoadColIds = new ArrayList<>();

    private static final String EXCEL_EXTENSION = ".xlsx";
    private static final String BDF_EXTENSION = ".bdf";
    private final Workbook workbook;
    private final Sheet sheetForces;
    private final Sheet sheetMultipliedForces;
    private final Sheet sheetConfig;

    private int CELL_ID_STARTING_LOAD = 6;
    private int ROW_ID_STARTING_LOAD_CARD = 10;

    public XlsxFile(String filePath) throws IOException {
        fileName = extractFileName(filePath);
        loadcols = new LoadcolContainer();
        loadcases = new LoadcaseContainer();

        this.filePath = filePath;
        filePathForNewBDF = convertToNewBDFFilePath(filePath);
        workbook = new XSSFWorkbook(this.filePath);

        sheetForces = workbook.getSheet("Forces");
        sheetMultipliedForces = workbook.getSheet("MultipliedForces");
        sheetConfig = workbook.getSheet("Config");

        if (sheetForces == null) {
            System.out.println("There is no 'Forces' card in xlsx file. Nothing was done.");
            setApplicationStatus("LC/BC: There is no 'Forces' card in xlsx file. Nothing was done.");
            AppRun.updateStatus("Finished.");
            AppRun.updateStatus("-------------------------------------");
            workbook.close();
            return;
        }
        //readLoadcasesAndLoadCols();
    }

    private String extractFileName(String filePath) {
        String fileNameWithExtension = filePath.replaceAll(".*/", "");
        return fileNameWithExtension.replaceAll("\\..*", "");
    }

    @Override
    public void readLoadcasesAndLoadCols() {
        AppRun.updateStatus("LC/BC: reading xlsx file...");
        Row row;
        Row currentRow;
        Cell cell;
        Cell currentCell;

        //--------------------------------------------------------------------------------------------------------------
        // read different load cards
        row = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD);    //ROW_ID_STARTING_LOAD_CARD = 10
        cell = row.getCell(CELL_ID_STARTING_LOAD);              //CELL_ID_STARTING_LOAD = 6;

        int id = CELL_ID_STARTING_LOAD - 1;
        while (true) {
            id++;
            currentCell = row.getCell(id);
            String stringCellValue;
            try {
                stringCellValue = currentCell.getStringCellValue();
            } catch (NullPointerException e) {
                break;
            }

            //--------------------------------------------------------------------------------------------------------------
            // read for GRAV
            if (stringCellValue.equals("GRAV")) {
                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD + 1);
                Cell cellLoadName = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 1);
                Cell cellLoadId = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 4);
                Cell cellLoadGravValue = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 5);
                Cell cellLoadGravXComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 6);
                Cell cellLoadGravYComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 7);
                Cell cellLoadGravZComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 8);
                Cell cellLoadGravHMColor = currentRow.getCell(id);

                String cellLoadNameString = getStringFromCell(cellLoadName, false);
                Double cellLoadIdDouble = cellLoadId.getNumericCellValue();
                Double cellLoadGravValueDouble = cellLoadGravValue.getNumericCellValue();
                Double cellLoadGravXComponentDouble = cellLoadGravXComponent.getNumericCellValue();
                Double cellLoadGravYComponentDouble = cellLoadGravYComponent.getNumericCellValue();
                Double cellLoadGravZComponentDouble = cellLoadGravZComponent.getNumericCellValue();
                Double cellLoadGravHMColorDouble = cellLoadGravHMColor.getNumericCellValue();

                // create loadcol grav
                Loadcol loadcolCurrent = new LoadcolGrav();
                loadcolCurrent.setLoadType(LoadType.GRAV);
                loadcolCurrent.setColorId(Integer.valueOf(cellLoadGravHMColorDouble.toString().replaceAll("\\..*", "")));
                loadcolCurrent.setDescription(cellLoadNameString);
                loadcolCurrent.setId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                if (!listOfAllLoadColIds.contains(loadcolCurrent.getId())) {
                    listOfAllLoadColIds.add(loadcolCurrent.getId());
                }
                loadcolCurrent.setFemId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                List<String> nastranTable = new ArrayList<>();

                nastranTable.add(loadcolCurrent.getLoadType().toString());
                nastranTable.add(loadcolCurrent.getId().toString());
                nastranTable.add(" ");
                nastranTable.add(cellLoadGravValueDouble.toString());
                nastranTable.add(cellLoadGravXComponentDouble.toString());
                nastranTable.add(cellLoadGravYComponentDouble.toString());
                nastranTable.add(cellLoadGravZComponentDouble.toString());
                nastranTable.add(" ");
                loadcolCurrent.setNastranTable(nastranTable);
                //$HMNAME LOADCOL              101"GRAV_Z_+1g"
                //$HWCOLOR LOADCOL             101      29
                //nastranTable=[GRAV    ,      102,         , 9810.0  , 1.0     , 0.0     , 0.0     ,         ]}

                loadcols.add(loadcolCurrent);
                //System.out.println(BDFprintTools.getBDFentry(loadcolCurrent));

            } else if (stringCellValue.equals("FORCE")) {
                //--------------------------------------------------------------------------------------------------------------
                // read for FORCE
                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD + 1);
                Cell cellLoadName = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 1);
                Cell cellLoadId = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 2);
                Cell cellLoadForceCID = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 4);
                Cell cellLoadForceFactor = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 5);
                Cell cellLoadForceXComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 6);
                Cell cellLoadForceYComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 7);
                Cell cellLoadForceZComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 8);
                Cell cellLoadForceHMColor = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 9);
                Cell cellLoadForceNodes = currentRow.getCell(id);

                String cellLoadNameString = getStringFromCell(cellLoadName, false);
                Double cellLoadIdDouble = cellLoadId.getNumericCellValue();
                String cellLoadForceCIDString = getStringFromCell(cellLoadForceCID, true);
                String cellLoadForceFactorString = getStringFromCell(cellLoadForceFactor, false);
                String cellLoadForceXComponentString = getStringFromCell(cellLoadForceXComponent, false);
                String cellLoadForceYComponentString = getStringFromCell(cellLoadForceYComponent, false);
                String cellLoadForceZComponentString = getStringFromCell(cellLoadForceZComponent, false);
                Double cellLoadForceHMColorDouble = cellLoadForceHMColor.getNumericCellValue();
                String cellLoadForceNodesString = getStringFromCell(cellLoadForceNodes, true);

                // create loadcol force
                String[] nodes = cellLoadForceNodesString.split(",");
                String[] forcesX = cellLoadForceXComponentString.split(",");
                String[] forcesY = cellLoadForceYComponentString.split(",");
                String[] forcesZ = cellLoadForceZComponentString.split(",");
                String[] cids = cellLoadForceCIDString.split(",");
                String[] factors = cellLoadForceFactorString.split(",");

                for (int i = 0; i < nodes.length; i++) {
                    Loadcol loadcolCurrent = new LoadcolForce();
                    loadcolCurrent.setDescription(cellLoadNameString);
                    loadcolCurrent.setLoadType(LoadType.FORCE);
                    loadcolCurrent.setFemId(Integer.valueOf(nodes[i]));
                    loadcolCurrent.setId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                    if (!listOfAllLoadColIds.contains(loadcolCurrent.getId())) {
                        listOfAllLoadColIds.add(loadcolCurrent.getId());
                    }
                    loadcolCurrent.setColorId(Integer.valueOf(cellLoadForceHMColorDouble.toString().replaceAll("\\..*", "")));

                    // create nastran table
                    List<String> nastranTable = new ArrayList<>();
                    nastranTable.add(loadcolCurrent.getLoadType().toString());
                    nastranTable.add(loadcolCurrent.getId().toString());
                    nastranTable.add(nodes[i]);

                    if (cids.length > 1) {
                        nastranTable.add(cids[i]);
                    } else {
                        nastranTable.add(cids[0]);
                    }

                    if (factors.length > 1) {
                        nastranTable.add(factors[i]);
                    } else {
                        nastranTable.add(factors[0]);
                    }

                    if (forcesX.length > 1) {
                        nastranTable.add(forcesX[i]);
                    } else {
                        nastranTable.add(forcesX[0]);
                    }

                    if (forcesY.length > 1) {
                        nastranTable.add(forcesY[i]);
                    } else {
                        nastranTable.add(forcesY[0]);
                    }

                    if (forcesZ.length > 1) {
                        nastranTable.add(forcesZ[i]);
                    } else {
                        nastranTable.add(forcesZ[0]);
                    }
                    loadcolCurrent.setNastranTable(nastranTable);
                    loadcols.add(loadcolCurrent);
                }
            } else if (stringCellValue.equals("MOMENT")) {
                //--------------------------------------------------------------------------------------------------------------
                // read for MOMENT
                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD + 1);
                Cell cellLoadName = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 1);
                Cell cellLoadId = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 2);
                Cell cellLoadMomentCID = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 4);
                Cell cellLoadMomentFactor = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 5);
                Cell cellLoadMomentXComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 6);
                Cell cellLoadMomentYComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 7);
                Cell cellLoadMomentZComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 8);
                Cell cellLoadMomentHMColor = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 9);
                Cell cellLoadMomentNodes = currentRow.getCell(id);

                String cellLoadNameString = getStringFromCell(cellLoadName, false);
                Double cellLoadIdDouble = cellLoadId.getNumericCellValue();
                String cellLoadMomentCIDString =  getStringFromCell(cellLoadMomentCID, true);
                String cellLoadMomentFactorString =  getStringFromCell(cellLoadMomentFactor, false);
                String cellLoadMomentXComponentString =  getStringFromCell(cellLoadMomentXComponent, false);
                String cellLoadMomentYComponentString =  getStringFromCell(cellLoadMomentYComponent, false);
                String cellLoadMomentZComponentString =  getStringFromCell(cellLoadMomentZComponent, false);
                Double cellLoadMomentHMColorDouble = cellLoadMomentHMColor.getNumericCellValue();
                String cellLoadMomentNodesString =  getStringFromCell(cellLoadMomentNodes, true);

                // create loadcol Moment
                String[] nodes = cellLoadMomentNodesString.split(",");
                String[] MomentsX = cellLoadMomentXComponentString.split(",");
                String[] MomentsY = cellLoadMomentYComponentString.split(",");
                String[] MomentsZ = cellLoadMomentZComponentString.split(",");
                String[] cids = cellLoadMomentCIDString.split(",");
                String[] factors = cellLoadMomentFactorString.split(",");

                for (int i = 0; i < nodes.length; i++) {
                    Loadcol loadcolCurrent = new LoadcolMoment();
                    loadcolCurrent.setDescription(cellLoadNameString);
                    loadcolCurrent.setLoadType(LoadType.MOMENT);
                    loadcolCurrent.setFemId(Integer.valueOf(nodes[i]));
                    loadcolCurrent.setId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                    if (!listOfAllLoadColIds.contains(loadcolCurrent.getId())) {
                        listOfAllLoadColIds.add(loadcolCurrent.getId());
                    }
                    loadcolCurrent.setColorId(Integer.valueOf(cellLoadMomentHMColorDouble.toString().replaceAll("\\..*", "")));

                    // create nastran table
                    List<String> nastranTable = new ArrayList<>();
                    nastranTable.add(loadcolCurrent.getLoadType().toString());
                    nastranTable.add(loadcolCurrent.getId().toString());
                    nastranTable.add(nodes[i]);

                    if (cids.length > 1) {
                        nastranTable.add(cids[i]);
                    } else {
                        nastranTable.add(cids[0]);
                    }

                    if (factors.length > 1) {
                        nastranTable.add(factors[i]);
                    } else {
                        nastranTable.add(factors[0]);
                    }

                    if (MomentsX.length > 1) {
                        nastranTable.add(MomentsX[i]);
                    } else {
                        nastranTable.add(MomentsX[0]);
                    }

                    if (MomentsY.length > 1) {
                        nastranTable.add(MomentsY[i]);
                    } else {
                        nastranTable.add(MomentsY[0]);
                    }

                    if (MomentsZ.length > 1) {
                        nastranTable.add(MomentsZ[i]);
                    } else {
                        nastranTable.add(MomentsZ[0]);
                    }
                    loadcolCurrent.setNastranTable(nastranTable);
                    loadcols.add(loadcolCurrent);
                }
            } else if (stringCellValue.equals("PLOAD4")) {
                //--------------------------------------------------------------------------------------------------------------
                // read for PLOAD4
                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD + 1);
                Cell cellLoadName = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 1);
                Cell cellLoadId = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 2);
                Cell cellLoadPload4CID = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 3);
                Cell cellLoadPload4Other = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 4);
                Cell cellLoadPload4Factor = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 5);
                Cell cellLoadPload4XComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 6);
                Cell cellLoadPload4YComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 7);
                Cell cellLoadPload4ZComponent = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 8);
                Cell cellLoadPload4HMColor = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 9);
                Cell cellLoadPload4Nodes = currentRow.getCell(id);

                String cellLoadNameString = getStringFromCell(cellLoadName, false);
                Double cellLoadIdDouble = cellLoadId.getNumericCellValue();
                String cellLoadPload4OtherString = getStringFromCell(cellLoadPload4Other, false);
                String cellLoadPload4CIDString = getStringFromCell(cellLoadPload4CID, true);
                String cellLoadPload4FactorString = getStringFromCell(cellLoadPload4Factor, false);
                String cellLoadPload4XComponentString = getStringFromCell(cellLoadPload4XComponent, false);
                String cellLoadPload4YComponentString = getStringFromCell(cellLoadPload4YComponent, false);
                String cellLoadPload4ZComponentString = getStringFromCell(cellLoadPload4ZComponent, false);
                Double cellLoadPload4HMColorDouble = cellLoadPload4HMColor.getNumericCellValue();
                String cellLoadPload4NodesString = getStringFromCell(cellLoadPload4Nodes, true);

                // create loadcol Pload4
                String[] nodes = cellLoadPload4NodesString.split(",");
                String[] Pload4sX = cellLoadPload4XComponentString.split(",");
                String[] Pload4sY = cellLoadPload4YComponentString.split(",");
                String[] Pload4sZ = cellLoadPload4ZComponentString.split(",");
                String[] others = cellLoadPload4OtherString.split(";");
                String[] cids = cellLoadPload4CIDString.split(",");
                String[] factors = cellLoadPload4FactorString.split(",");

                for (int i = 0; i < nodes.length; i++) {
                    Loadcol loadcolCurrent = new LoadcolPload4();
                    loadcolCurrent.setDescription(cellLoadNameString);
                    loadcolCurrent.setLoadType(LoadType.PLOAD4);
                    loadcolCurrent.setFemId(Integer.valueOf(nodes[i]));
                    loadcolCurrent.setId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                    if (!listOfAllLoadColIds.contains(loadcolCurrent.getId())) {
                        listOfAllLoadColIds.add(loadcolCurrent.getId());
                    }
                    loadcolCurrent.setColorId(Integer.valueOf(cellLoadPload4HMColorDouble.toString().replaceAll("\\..*", "")));

                    // create nastran table
                    List<String> nastranTable = new ArrayList<>();
                    nastranTable.add(loadcolCurrent.getLoadType().toString());
                    nastranTable.add(loadcolCurrent.getId().toString());
                    nastranTable.add(nodes[i]);

                    if (factors.length > 1) {
                        nastranTable.add(factors[i]);
                    } else {
                        nastranTable.add(factors[0]);
                    }

                    if (others.length > 1) {
                        String[] othersCurrent = others[i].split(",");
                        for (String other : othersCurrent) {
                            nastranTable.add(other);
                        }
                    } else {
                        String[] othersCurrent = others[0].split(",");
                        for (String other : othersCurrent) {
                            nastranTable.add(other);
                        }
                    }

                    if (cids.length > 1) {
                        nastranTable.add(cids[i]);
                    } else {
                        nastranTable.add(cids[0]);
                    }

                    if (Pload4sX.length > 1) {
                        nastranTable.add(Pload4sX[i]);
                    } else {
                        nastranTable.add(Pload4sX[0]);
                    }

                    if (Pload4sY.length > 1) {
                        nastranTable.add(Pload4sY[i]);
                    } else {
                        nastranTable.add(Pload4sY[0]);
                    }

                    if (Pload4sZ.length > 1) {
                        nastranTable.add(Pload4sZ[i]);
                    } else {
                        nastranTable.add(Pload4sZ[0]);
                    }
                    loadcolCurrent.setNastranTable(nastranTable);
                    loadcols.add(loadcolCurrent);
                }
            } else if (stringCellValue.equals("SPC")) {
                //--------------------------------------------------------------------------------------------------------------
                // read for SPC
                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD + 1);
                Cell cellLoadName = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 1);
                Cell cellLoadId = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 3);
                Cell cellLoadSPCdofs = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 4);
                Cell cellLoadSPCvalue = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 8);
                Cell cellLoadSPCHMColor = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 9);
                Cell cellLoadSPCNodes = currentRow.getCell(id);

                String cellLoadNameString = getStringFromCell(cellLoadName, false);
                Double cellLoadIdDouble = cellLoadId.getNumericCellValue();
                String cellLoadSPCdofsString = getStringFromCell(cellLoadSPCdofs, true);
                String cellLoadSPCvalueString = getStringFromCell(cellLoadSPCvalue, false);
                Double cellLoadSPCHMColorDouble = cellLoadSPCHMColor.getNumericCellValue();
                String cellLoadSPCNodesString = getStringFromCell(cellLoadSPCNodes, true);

                // create loadcol SPC
                String[] nodes = cellLoadSPCNodesString.split(",");
                String[] dofs = cellLoadSPCdofsString.split(",");
                String[] values = cellLoadSPCvalueString.split(",");

                for (int i = 0; i < nodes.length; i++) {
                    Loadcol loadcolCurrent = new LoadcolSpc();
                    loadcolCurrent.setDescription(cellLoadNameString);
                    loadcolCurrent.setLoadType(LoadType.SPC);
                    loadcolCurrent.setFemId(Integer.valueOf(nodes[i]));
                    loadcolCurrent.setId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                    if (!listOfAllLoadColIds.contains(loadcolCurrent.getId())) {
                        listOfAllLoadColIds.add(loadcolCurrent.getId());
                    }
                    loadcolCurrent.setColorId(Integer.valueOf(cellLoadSPCHMColorDouble.toString().replaceAll("\\..*", "")));

                    // create nastran table
                    List<String> nastranTable = new ArrayList<>();
                    nastranTable.add(loadcolCurrent.getLoadType().toString());
                    nastranTable.add(loadcolCurrent.getId().toString());
                    nastranTable.add(nodes[i]);

                    if (dofs.length > 1) {
                        nastranTable.add(dofs[i]);
                    } else {
                        nastranTable.add(dofs[0]);
                    }

                    if (values.length > 1) {
                        nastranTable.add(values[i]);
                    } else {
                        nastranTable.add(values[0]);
                    }
                    loadcolCurrent.setNastranTable(nastranTable);
                    loadcols.add(loadcolCurrent);
                }
            } else if (stringCellValue.equals("SPCADD")) {
                //--------------------------------------------------------------------------------------------------------------
                // read for SPCADD
                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD + 1);
                Cell cellLoadName = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 1);
                Cell cellLoadId = currentRow.getCell(id);

                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 3);
                Cell cellLoadSPCADDcomponentSpcs = currentRow.getCell(id);


                currentRow = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD - 8);
                Cell cellLoadSPCHMColor = currentRow.getCell(id);


                String cellLoadNameString = getStringFromCell(cellLoadName, false);
                Double cellLoadIdDouble = cellLoadId.getNumericCellValue();
                String cellLoadSPCADDcomponentSpcsString = getStringFromCell(cellLoadSPCADDcomponentSpcs, true);
                Double cellLoadSPCHMColorDouble = cellLoadSPCHMColor.getNumericCellValue();

                // create loadcol SPCADD
                String[] dofs = cellLoadSPCADDcomponentSpcsString.split(",");

                List<Integer> availableSpcIds = new ArrayList<>();
                for (Loadcol spc : loadcols.getLoadcolSpcs()) {
                    Integer spcId = spc.getId();
                    availableSpcIds.add(spcId);
                }
                boolean spcaddHaveSpcWhichDoesNotExist = false;
                int problematicSpc = 0;
                for (String dof : dofs) {
                    if (dof.equals("")) {
                        continue;
                    }
                    int dofInteger = Integer.parseInt(dof);
                    if (!availableSpcIds.contains(dofInteger)) {
                        spcaddHaveSpcWhichDoesNotExist = true;
                        problematicSpc = dofInteger;
                        break;
                    }
                }
                if (spcaddHaveSpcWhichDoesNotExist) {
                    System.out.println("Given SPC :" + problematicSpc
                            + " used in SPCADD: " + cellLoadNameString + " does not exist." +
                            " This SPCADD was not created.");
                    continue;
                }

                Loadcol loadcolCurrent = new LoadcolSpcAdd();
                loadcolCurrent.setDescription(cellLoadNameString);
                loadcolCurrent.setLoadType(LoadType.SPCADD);
                loadcolCurrent.setFemId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                loadcolCurrent.setId(Integer.valueOf(cellLoadIdDouble.toString().replaceAll("\\..*", "")));
                if (!listOfAllLoadColIds.contains(loadcolCurrent.getId())) {
                    listOfAllLoadColIds.add(loadcolCurrent.getId());
                }
                loadcolCurrent.setColorId(Integer.valueOf(cellLoadSPCHMColorDouble.toString().replaceAll("\\..*", "")));

                // create nastran table
                List<String> nastranTable = new ArrayList<>();
                nastranTable.add(loadcolCurrent.getLoadType().toString());
                nastranTable.add(loadcolCurrent.getId().toString());

                if (dofs.length > 1) {
                    for (String dof : dofs) {
                        nastranTable.add(dof);
                    }
                } else {
                    nastranTable.add(dofs[0]);
                }
                loadcolCurrent.setNastranTable(nastranTable);
                loadcols.add(loadcolCurrent);
            }


        }


//        List<Loadcol> loadcolGravs = loadcols.getLoadcolGravs();
//        for (Loadcol loadcol : loadcolGravs) {
//            System.out.println(BDFprintTools.getBDFentry(loadcol));
//        }
//
//        List<Loadcol> loadcolForces = loadcols.getLoadcolForces();
//        for(Loadcol loadcol : loadcolForces) {
//            //System.out.println(loadcol.toString());
//            System.out.println(BDFprintTools.getBDFentry(loadcol));
//        }
//
//        List<Loadcol> loadcolPload4s = loadcols.getLoadcolPload4s();
//        for (Loadcol loadcol : loadcolPload4s) {
//            //System.out.println(loadcol.toString());
//            System.out.println(BDFprintTools.getBDFentry(loadcol));
//        }
//
//        List<Loadcol> loadcolSPCs = loadcols.getLoadcolSpcs();
//        for (Loadcol loadcol : loadcolSPCs) {
////            System.out.println(loadcol.toString());
//            System.out.println(BDFprintTools.getBDFentry(loadcol));
//        }
//
//        List<Loadcol> loadcolSPCADDs = loadcols.getLoadcolSpcAdds();
//        for (Loadcol loadcol : loadcolSPCADDs) {
////            System.out.println(loadcol.toString());
//            System.out.println(BDFprintTools.getBDFentry(loadcol));
//        }

        // =============================================================================================================
        // read loadcases

        // check maximal id of loadcols
        //System.out.println(listOfAllLoadColIds.toString());
        int maxId = getMaxIdFromLoadCols();
        //System.out.println("maxid = " + maxId);
        int startingLoadId = getStartingLoadId(maxId);
        //System.out.println("starting load id = " + startingLoadId);
        List<Integer> lcLoadIds = new ArrayList<>();
        List<Integer> lcSpcIds = new ArrayList<>();

        // create loadcase
        row = sheetForces.getRow(ROW_ID_STARTING_LOAD_CARD + 2);    //ROW_ID_STARTING_LOAD_CARD = 10
        cell = row.getCell(CELL_ID_STARTING_LOAD - 1);              //CELL_ID_STARTING_LOAD = 6;

        int currentRowId = ROW_ID_STARTING_LOAD_CARD + 1;
        int currentCellId = CELL_ID_STARTING_LOAD - 1;
        while (true) {
            currentRowId++;
            currentCellId = CELL_ID_STARTING_LOAD - 1;
            row = sheetForces.getRow(currentRowId);

            try {
                currentCell = row.getCell(currentCellId);
            } catch (NullPointerException e) {
                break;
            }
            Loadcase currentLoadCase = new Loadcase();

            // read loadcase name
            String currentLoadCaseNameString = currentCell.getStringCellValue();
            currentLoadCase.setLabel(currentLoadCaseNameString);
            currentLoadCase.setHmDescription(currentLoadCaseNameString);

            // read loadcase id
            currentCell = row.getCell(--currentCellId);
            //TODO if cell is empty, create new loadcase id
            double lcId = currentCell.getNumericCellValue();
            currentLoadCase.setId((int) lcId);

            // read load id
            currentCell = row.getCell(--currentCellId);
            //TODO if cell is empty, create new LOAD ID
            double loadId = currentCell.getNumericCellValue();
            currentLoadCase.setLoadId((int) loadId);

            // --- CREATION OF LOAD CARD
            Loadcol load = new LoadcolLoad();
            load.setLoadType(LoadType.LOAD);
            load.setColorId(3); // red
            load.setId((int) loadId);
            load.setDescription("autoLOAD_" + load.getId());
            List<String> loadNastranTable = new ArrayList<>();
            loadNastranTable.add(load.getLoadType().toString());
            loadNastranTable.add(load.getId().toString());

            //set global load factor
            currentCell = row.getCell(--currentCellId);
            double loadGLF = currentCell.getNumericCellValue();
            loadNastranTable.add(String.valueOf((int) loadGLF));

            // go back to load id
            currentCell = row.getCell(++currentCellId);

            int currentCellIdForLoad = currentCellId;
            currentCellIdForLoad = currentCellIdForLoad + 2;
            while (true) {
                currentCellIdForLoad++;
                currentCell = row.getCell(currentCellIdForLoad);
                double numericCellValue = 0;
                try {
                    numericCellValue = currentCell.getNumericCellValue();
                } catch (Exception e) {
                    break;
                }
                if (numericCellValue != 0) {
                    // check if is SPC
                    row = sheetForces.getRow(10);
                    currentCell = row.getCell(currentCellIdForLoad);
                    if (currentCell.getStringCellValue().equals("SPC")) {
                        continue;
                    }

                    // get factor
                    row = sheetForces.getRow(currentRowId);
                    currentCell = row.getCell(currentCellIdForLoad);
                    numericCellValue = currentCell.getNumericCellValue();
                    loadNastranTable.add(String.valueOf(numericCellValue));

                    // get load id
                    row = sheetForces.getRow(9); //
                    currentCell = row.getCell(currentCellIdForLoad);
                    numericCellValue = currentCell.getNumericCellValue();
                    loadNastranTable.add(String.valueOf((int) numericCellValue));

                    row = sheetForces.getRow(currentRowId);
                }

            }
            load.setNastranTable(loadNastranTable);
            loadcols.add(load);
            // --- END CREATION OF LOAD CARD

            // go back to load id
            row = sheetForces.getRow(currentRowId);
            currentCell = row.getCell(currentCellId);

            // read spc id
            --currentCellId;
            //TODO if cell is empty, then create own SPCADD or check if some SPC be should be used
            currentCell = row.getCell(--currentCellId);
            double spcId = currentCell.getNumericCellValue();
            currentLoadCase.setSpcId((int) spcId);

            loadcases.add(currentLoadCase);
        }


//        List<Loadcase> listOfLoadcases = loadcases.getList();
//        for (Loadcase loadcase : listOfLoadcases) {
//            //System.out.println("LOADCASES: " + loadcase);
//            System.out.println(BDFprintTools.getBDFentry(loadcase));
//        }

//        List<Loadcol> listOfLoads = loadcols.getLoadcolLoads();
//        for (Loadcol load : listOfLoads) {
//            //System.out.println("LOAD: " + load);
//            System.out.println(BDFprintTools.getBDFentry(load));
//        }

        closeWorkbook();
    }

    private String getStringFromCell(Cell cell, boolean splitByComaAndTakeTheFirstString) {
        if(cell.getCellType().equals(CellType.STRING)) {
            return cell.getStringCellValue();
        } else if(cell.getCellType().equals(CellType.NUMERIC)) {
            if(splitByComaAndTakeTheFirstString) {
                return String.valueOf(cell.getNumericCellValue()).split("\\.")[0];
            } else {
                return String.valueOf(cell.getNumericCellValue());
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "There was problem with value conversion for cell: " +
                    "\n" + cell.getAddress() + "\n\nValue 99999999 was assumed for that.");
            return "99999999";
        }
    }

    private int getMaxIdFromLoadCols() {
        int max = 0;
        for (int id : listOfAllLoadColIds) {
            max = id > max ? id : max;
        }
        return max;
    }

    private int getStartingLoadId(int maxId) {
        if (maxId < 1000) {
            return 1000;
        }

        String maxIdString = String.valueOf(maxId);
        int length = maxIdString.length();
        int firstDigit = Integer.parseInt(maxIdString.substring(0, 1));

        int newFirstDigit = ++firstDigit;
        StringBuffer result = new StringBuffer();
        result.append(newFirstDigit);

        for (int i = 1; i < length; i++) {
            result.append("0");
        }

        return Integer.parseInt(result.toString());
    }


    @Override
    public void updateLoadCols() {
    }

    private void closeWorkbook() {
        try {
            workbook.close();
            AppRun.updateStatus("LC/BC: reading xlsx file - done.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setApplicationStatus(String newStatus) {
        AppRun.updateStatus(newStatus);
    }

    @Override
    public void writeToFile() {
        AppRun.updateStatus("LC/BC: writing to bdf file...");

        StringBuffer bdfPrint = new StringBuffer();

//        bdfPrint.append("$\n");
//        bdfPrint.append("$ LOADCASES -----------------------------------------\n");
//        bdfPrint.append("$\n");
        List<Loadcase> listOfLoadcases = loadcases.getList();
        for (Loadcase loadcase : listOfLoadcases) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcase));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$ LOADS: GRAV ---------------------------------------\n");
        bdfPrint.append("$\n");
        List<Loadcol> listOfLoadcols = loadcols.getLoadcolGravs();
        for (Loadcol loadcol : listOfLoadcols) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcol));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$ LOADS: FORCE --------------------------------------\n");
        bdfPrint.append("$\n");
        listOfLoadcols = loadcols.getLoadcolForces();
        for (Loadcol loadcol : listOfLoadcols) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcol));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$ LOADS: MOMENT -------------------------------------\n");
        bdfPrint.append("$\n");
        listOfLoadcols = loadcols.getLoadcolMoment();
        for (Loadcol loadcol : listOfLoadcols) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcol));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$ LOADS: PLOAD4 -------------------------------------\n");
        bdfPrint.append("$\n");
        listOfLoadcols = loadcols.getLoadcolPload4s();
        for (Loadcol loadcol : listOfLoadcols) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcol));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$ LOADS: LOAD ---------------------------------------\n");
        bdfPrint.append("$\n");
        listOfLoadcols = loadcols.getLoadcolLoads();
        for (Loadcol loadcol : listOfLoadcols) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcol));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$ BC: SPC -------------------------------------------\n");
        bdfPrint.append("$\n");
        listOfLoadcols = loadcols.getLoadcolSpcs();
        for (Loadcol loadcol : listOfLoadcols) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcol));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$ BC: SPCADD ----------------------------------------\n");
        bdfPrint.append("$\n");
        listOfLoadcols = loadcols.getLoadcolSpcAdds();
        for (Loadcol loadcol : listOfLoadcols) {
            bdfPrint.append(BDFprintTools.getBDFentry(loadcol));
        }
        bdfPrint.append("$\n");
        bdfPrint.append("$\n");

//        System.out.println(bdfPrint.toString());
        // write to file


        try (BufferedWriter bf = new BufferedWriter(new FileWriter(filePathForNewBDF))) {
            bf.write(bdfPrint.toString());

            System.out.println("LC/BC: File was written: " + filePathForNewBDF);
            AppRun.updateStatus("LC/BC: File was written: " + filePathForNewBDF);
            AppRun.updateStatus("Finished.");
            AppRun.updateStatus("-------------------------------------");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private String convertToNewBDFFilePath(String filePath) {
        String fileName = filePath.replaceAll(".*/", "");
        String pathOnly = filePath.replaceAll(fileName, "");
        String nameOnly = fileName.replaceAll("\\..*", "");

        LocalTime localTime = LocalTime.now();
        String dateToPrint = "_" + localTime.getHour() + "" + localTime.getMinute() + "" + localTime.getSecond() + "_" + System.getProperty("user.name").toUpperCase();

        String result = pathOnly + nameOnly + dateToPrint + BDF_EXTENSION;
        return result;
    }

}
