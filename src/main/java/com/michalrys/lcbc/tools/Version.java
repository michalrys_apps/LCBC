package com.michalrys.lcbc.tools;

import com.michalrys.lcbc.AppRun;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;

public class Version {
    public static final String MAIN_LOCATION_ON_SERVER = "//ece508/CAE_2/CAE_Baza_wiedzy/2018-02-07_MR_macros_MR/LCBC_MR/";
    public static final String VERSION_ACTUAL_ON_SERVER = "//ece508/CAE_2/CAE_Baza_wiedzy/2018-02-07_MR_macros_MR/LCBC_MR/OTHER/ACTUAL/";
    private final static String FILE_NAME = "LCBC_MR_ActualVersion.json";

    //public static boolean isValid(String actualVersionFolderPath, String thisVersion, String actualAppLocation) {
    public static boolean isValid(String thisVersion) {
        String fileRead = "";

        try (BufferedReader br = new BufferedReader(new FileReader(VERSION_ACTUAL_ON_SERVER + FILE_NAME))) {
            String line;
            while ((line = br.readLine()) != null) {
                fileRead += line + "\n";
            }

        } catch (Exception e) {
            System.out.println("OK, use this version. It is not possible to check if this version is actual.");
            AppRun.updateStatus("LC/BC: Ok, use this version. It is not possible to check if this version is actual.");
            return true;
        }

        JSONObject actualVersionJsonData = new JSONObject(fileRead);
        String actualVersionRead = actualVersionJsonData.getString("Actual version of LCBC_MR is");

        if (!thisVersion.equals(actualVersionRead)) {
            AppRun.updateStatus("LC/BC: This version is not actual. Please use new version.");
            JFrame jFrame = new JFrame("Version check");
            jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            jFrame.setLocation(300, 300);
            jFrame.setSize(500, 120);

            JTextArea jTextArea = new JTextArea("" + "" +
                    "  This version : " + thisVersion + " is not actual." +
                    "\n  It is not allowed to use it. Please go to :" +
                    "\n    " + MAIN_LOCATION_ON_SERVER +
                    "\n  There is actual version: " + actualVersionRead + ".");

            jTextArea.setEditable(false);
            jTextArea.setFont(new Font("Arial", Font.PLAIN, 12));
            jFrame.getContentPane().add(jTextArea, BorderLayout.CENTER);
            jFrame.setVisible(true);
            jFrame.setAlwaysOnTop(true);

            return false;
        }
        return true;
    }
}
