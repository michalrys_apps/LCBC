package com.michalrys.lcbc.tools;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;

public class Statistics{
    public static final String STATISTICS_ON_SERVER = "//ece508/CAE_2/CAE_Baza_wiedzy/2018-02-07_MR_macros_MR/LCBC_MR/OTHER/STATS/";

    public static void writeData() {
        StringBuilder fileStatisticPath = new StringBuilder();
        fileStatisticPath.append(STATISTICS_ON_SERVER);
        fileStatisticPath.append("LCBC_MR_stats_");
        fileStatisticPath.append(LocalDate.now().toString());
        fileStatisticPath.append(".csv");

        System.out.println(fileStatisticPath.toString());


        try (BufferedWriter bf = new BufferedWriter(new FileWriter(fileStatisticPath.toString(), true))) {
            bf.write(System.getProperty("user.name") + ";" +
                    LocalDate.now().toString() + ";" +
                    LocalTime.now().toString() + ";" +
                    "1\n");
            System.out.println("Stats were written.");
        } catch (IOException exc) {
            System.out.println("Stats were not written.");
        }
    }
}
