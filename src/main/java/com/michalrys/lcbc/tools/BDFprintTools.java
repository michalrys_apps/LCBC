package com.michalrys.lcbc.tools;

import com.michalrys.lcbc.loadcase.Loadcase;
import com.michalrys.lcbc.loadcols.LoadType;
import com.michalrys.lcbc.loadcols.Loadcol;

import java.util.List;

public class BDFprintTools {
    public BDFprintTools() {
    }

    public static String getNastranTablePrintable(Loadcol loadcol) {
        StringBuffer results = new StringBuffer();
        if (loadcol.getLoadType().equals(LoadType.GRAV) ||
                loadcol.getLoadType().equals(LoadType.FORCE) ||
                loadcol.getLoadType().equals(LoadType.MOMENT) ||
                loadcol.getLoadType().equals(LoadType.SPC)) {
            List<String> nastranTable = loadcol.getNastranTable();
            for (String cell : nastranTable) {
                results.append(cell);
                int length = cell.length();
                for (int i = 1; i <= 8 - length; i++) {
                    results.append(" ");
                }
            }
        } else if (loadcol.getLoadType().equals(LoadType.PLOAD4)) {
            List<String> nastranTable = loadcol.getNastranTable();
            int howManyCellsInNastranTable = 0;
            for (String cell : nastranTable) {
                ++howManyCellsInNastranTable;

                if (cell.length() != 0) {
                    results.append(cell);
                    int length = cell.length();
                    for (int i = 1; i <= 8 - length; i++) {
                        results.append(" ");
                    }
                } else {          //12345678
                    results.append("        ");
                }

                if (howManyCellsInNastranTable % 10 == 0) {
                    results.append("\n");
                }
            }
        } else if (loadcol.getLoadType().equals(LoadType.SPCADD)) {
            List<String> nastranTable = loadcol.getNastranTable();
            int howManyCellsInNastranTable = 0;
            for (String cell : nastranTable) {
                ++howManyCellsInNastranTable;

                if (cell.length() != 0) {
                    results.append(cell);
                    int length = cell.length();
                    for (int i = 1; i <= 8 - length; i++) {
                        results.append(" ");
                    }
                } else {            //12345678
                    results.append("        ");
                }

                if (howManyCellsInNastranTable % 9 == 0) {
                    results.append("\n");
                }
            }
        } else if (loadcol.getLoadType().equals(LoadType.LOAD)) {
            List<String> nastranTable = loadcol.getNastranTable();
            int howManyCellsInNastranTable = 0;
            for (String cell : nastranTable) {
                ++howManyCellsInNastranTable;

                if (cell.length() != 0) {
                    results.append(cell);
                    int length = cell.length();
                    for (int i = 1; i <= 8 - length; i++) {
                        results.append(" ");
                    }
                } else {          //12345678
                    results.append("        ");
                }

                if (howManyCellsInNastranTable % 9 == 0) {
                    results.append("\n+       ");
                }
            }
        }


        return results.toString();
    }

    public static String getBDFentry(Loadcol loadcol) {
        StringBuffer results = new StringBuffer();
        if (loadcol.getLoadType().equals(LoadType.GRAV) ||
                loadcol.getLoadType().equals(LoadType.FORCE) ||
                loadcol.getLoadType().equals(LoadType.MOMENT) ||
                loadcol.getLoadType().equals(LoadType.PLOAD4) ||
                loadcol.getLoadType().equals(LoadType.SPC) ||
                loadcol.getLoadType().equals(LoadType.SPCADD) ||
                loadcol.getLoadType().equals(LoadType.LOAD)) {
            results.append("$HMNAME LOADCOL         ");

            int idLength = loadcol.getId().toString().length();
            for (int i = 1; i <= 8 - idLength; i++) {
                results.append(" ");
            }
            results.append(loadcol.getId());
            results.append("\"");
            results.append(loadcol.getDescription());
            results.append("\"");
            results.append("\n");
            //---------------------------------------------------
            results.append("$HWCOLOR LOADCOL        ");
            for (int i = 1; i <= 8 - idLength; i++) {
                results.append(" ");
            }
            results.append(loadcol.getId());
            int colorLength = loadcol.getColorId().toString().length();
            for (int i = 1; i <= 8 - colorLength; i++) {
                results.append(" ");
            }
            results.append(loadcol.getColorId());
            results.append("\n");
            //---------------------------------------------------
            results.append(getNastranTablePrintable(loadcol));
            results.append("\n");
        }
        return results.toString();
    }

    public static String getBDFentry(Loadcase loadcase) {
        StringBuffer results = new StringBuffer();

        results.append("$HMNAME LOADSTEP        ");

        int idLength = loadcase.getId().toString().length();
        for (int i = 1; i <= 8 - idLength; i++) {
            results.append(" ");
        }
        results.append(loadcase.getId());
        results.append("\"");
        results.append(loadcase.getHmDescription());
        results.append("\"");
        results.append("\n");
        //---------------------------------------------------
        results.append("SUBCASE ");
        idLength = loadcase.getId().toString().length();
        for (int i = 1; i <= 8 - idLength; i++) {
            results.append(" ");
        }
        results.append(loadcase.getId());
        results.append("\n");
        //---------------------------------------------------
        results.append("  LABEL= ");
        results.append(loadcase.getLabel());
        results.append("\n");
        //---------------------------------------------------
        results.append("  SPC = ");
        results.append(loadcase.getSpcId());
        results.append("\n");
        //---------------------------------------------------
        results.append("  LOAD = ");
        results.append(loadcase.getLoadId());
        results.append("\n$\n");

        return results.toString();
    }
}
