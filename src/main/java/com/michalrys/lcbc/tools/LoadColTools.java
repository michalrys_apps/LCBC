package com.michalrys.lcbc.tools;

import com.michalrys.lcbc.loadcols.LoadType;
import com.michalrys.lcbc.loadcols.Loadcol;

import java.util.ArrayList;
import java.util.List;

public class LoadColTools {

    private static final int EXCELL_CELL_CHARACTER_LIMITATION_32_767 = 30_000;
    public static final String EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO = "CHARACTERS LIMIT REACHED";

    public static String getNodes(Loadcol currentLoadcol, List<Loadcol> loadcols) {
        if (loadcols.get(0).getLoadType() == LoadType.GRAV ||
                loadcols.get(0).getLoadType() == LoadType.LOAD) {
            return "";
        }

        StringBuffer result = new StringBuffer();

        for (Loadcol loadcol : loadcols) {
            if (!currentLoadcol.getDescription().equals(loadcol.getDescription())) {
                continue;
            }
            result.append(loadcol.getNastranTable().get(2).trim() + ",");
            if (result.length() >= EXCELL_CELL_CHARACTER_LIMITATION_32_767 + 1) {
                break;
            }
        }
        result.replace(result.length() - 1, result.length(), "");

        // excel cell limitation is 32,767 characters
        if (result.length() >= EXCELL_CELL_CHARACTER_LIMITATION_32_767) {
            //return result.substring(0, EXCELL_CELL_CHARACTER_LIMITATION_32_767);
            return EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO + "," + result.substring(0, EXCELL_CELL_CHARACTER_LIMITATION_32_767);
        }

        return result.toString();
    }

    public static String getForceComponents(Loadcol currentLoadcol, String component, List<Loadcol> loadcols) {
        if (loadcols.get(0).getLoadType() != LoadType.FORCE) {
            return "";
        }

        int dataToRead;
        if (component.equals("Fx")) {
            dataToRead = 5;
        } else if (component.equals("Fy")) {
            dataToRead = 6;
        } else if (component.equals("Fz")) {
            dataToRead = 7;
        } else if (component.equals("CID")) {
            dataToRead = 3;
        } else if (component.equals("factor")) {
            dataToRead = 4;
        } else {
            dataToRead = 5;
        }

        StringBuffer result = new StringBuffer();
        List<String> listOfForces = new ArrayList<>();
        boolean theSame = true;
        boolean moreThanOne = false;

        for (Loadcol loadcol : loadcols) {
            if (!loadcol.getDescription().equals(currentLoadcol.getDescription())) {
                continue;
            }
            if (moreThanOne && !listOfForces.contains(loadcol.getNastranTable().get(dataToRead))) {
                theSame = false;
            }
            listOfForces.add(loadcol.getNastranTable().get(dataToRead));
            moreThanOne = true;
        }

        if (theSame) {
            return listOfForces.get(0).trim();
        } else {
            // excel cell limitation is 32,767 characters
            if (listOfForces.toString().replaceAll("[ \\[\\]]", "").length() >= EXCELL_CELL_CHARACTER_LIMITATION_32_767) {
                String substring = listOfForces.toString().replaceAll("[ \\[\\]]", "").substring(0, EXCELL_CELL_CHARACTER_LIMITATION_32_767);
                return EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO + "," + substring.replaceAll(",[0-9\\\\.]*$", "");
            }
            return listOfForces.toString().replaceAll("[ \\[\\]]", "");
        }
    }

    public static String getMomentComponents(Loadcol currentLoadcol, String component, List<Loadcol> loadcols) {
        if (loadcols.get(0).getLoadType() != LoadType.MOMENT) {
            return "";
        }

        int dataToRead;
        if (component.equals("Mx")) {
            dataToRead = 5;
        } else if (component.equals("My")) {
            dataToRead = 6;
        } else if (component.equals("Mz")) {
            dataToRead = 7;
        } else if (component.equals("CID")) {
            dataToRead = 3;
        } else if (component.equals("factor")) {
            dataToRead = 4;
        } else {
            dataToRead = 5;
        }

        StringBuffer result = new StringBuffer();
        List<String> listOfForces = new ArrayList<>();
        boolean theSame = true;
        boolean moreThanOne = false;

        for (Loadcol loadcol : loadcols) {
            if (!loadcol.getDescription().equals(currentLoadcol.getDescription())) {
                continue;
            }
            if (moreThanOne && !listOfForces.contains(loadcol.getNastranTable().get(dataToRead))) {
                theSame = false;
            }
            listOfForces.add(loadcol.getNastranTable().get(dataToRead));
            moreThanOne = true;
        }

        if (theSame) {
            return listOfForces.get(0).trim();
        } else {

            // excel cell limitation is 32,767 characters
            if (listOfForces.toString().replaceAll("[ \\[\\]]", "").length() >= EXCELL_CELL_CHARACTER_LIMITATION_32_767) {
                String substring = listOfForces.toString().replaceAll("[ \\[\\]]", "").substring(0, EXCELL_CELL_CHARACTER_LIMITATION_32_767);
                return EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO + "," + substring.replaceAll(",[0-9\\\\.]*$", "");
            }

            return listOfForces.toString().replaceAll("[ \\[\\]]", "");
        }
    }

    public static String getPload4sComponents(Loadcol currentLoadcol, String component, List<Loadcol> loadcols) {
        if (loadcols.get(0).getLoadType() != LoadType.PLOAD4) {
            return "";
        }

        int dataToRead;
        if (component.equals("GlobalFactor")) {
            dataToRead = 3;
        } else if (component.equals("Other")) {
            dataToRead = 4;
        } else if (component.equals("CID")) {
            dataToRead = 11;
        } else if (component.equals("N1")) {
            dataToRead = 12;
        } else if (component.equals("N2")) {
            dataToRead = 13;
        } else if (component.equals("N3")) {
            dataToRead = 14;
        } else {
            dataToRead = 3;
        }

        StringBuffer result = new StringBuffer();
        List<String> listOfForces = new ArrayList<>();
        boolean theSame = true;
        boolean moreThanOne = false;

        for (Loadcol loadcol : loadcols) {
            if (!loadcol.getDescription().equals(currentLoadcol.getDescription())) {
                continue;
            }
            if (moreThanOne && !listOfForces.contains(loadcol.getNastranTable().get(dataToRead))) {
                theSame = false;
            }
            if (dataToRead == 4) {
                String value4 = loadcol.getNastranTable().get(dataToRead);
                String value5 = loadcol.getNastranTable().get(dataToRead + 1).trim();
                String value6 = loadcol.getNastranTable().get(dataToRead + 2).trim();
                String value7 = loadcol.getNastranTable().get(dataToRead + 3).trim();
                String value8 = loadcol.getNastranTable().get(dataToRead + 4).trim();
                String value9 = loadcol.getNastranTable().get(dataToRead + 5).trim();
                String value10 = loadcol.getNastranTable().get(dataToRead + 6).trim();
                listOfForces.add("" +
                        value4 + "," +
                        value5 + "," +
                        value6 + "," +
                        value7 + "," +
                        value8 + "," +
                        value9 + "," +
                        value10 + ";");
                moreThanOne = true;
            } else {
                listOfForces.add(loadcol.getNastranTable().get(dataToRead));
                moreThanOne = true;
            }
        }
        if (theSame) {
            return listOfForces.get(0).trim();
        } else {

            // excel cell limitation is 32,767 characters
            if (listOfForces.toString().replaceAll("[ \\[\\]]", "").length() >= EXCELL_CELL_CHARACTER_LIMITATION_32_767) {
                String substring = listOfForces.toString().replaceAll("[ \\[\\]]", "").substring(0, EXCELL_CELL_CHARACTER_LIMITATION_32_767);
                return EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO + "," + substring.replaceAll(",[0-9\\\\.]*$", "");
            }

            //return listOfForces.toString().replaceAll("[ \\[\\]]", "");
            return listOfForces.toString().replaceAll(";,", ";").replaceAll("[ \\[\\]]", "");
        }
    }

    public static String getSPCComponents(Loadcol currentLoadcol, String component, List<Loadcol> loadcols) {
        if (loadcols.get(0).getLoadType() != LoadType.SPC) {
            return "";
        }

        int dataToRead;
        if (component.equals("Direction")) {
            dataToRead = 3;
        } else if (component.equals("Value")) {
            dataToRead = 4;
        } else {
            dataToRead = 3;
        }

        StringBuffer result = new StringBuffer();
        List<String> listOfForces = new ArrayList<>();
        boolean theSame = true;
        boolean moreThanOne = false;

        for (Loadcol loadcol : loadcols) {
            if (!loadcol.getDescription().equals(currentLoadcol.getDescription())) {
                continue;
            }
            if (moreThanOne && !listOfForces.contains(loadcol.getNastranTable().get(dataToRead))) {
                theSame = false;
            }
            listOfForces.add(loadcol.getNastranTable().get(dataToRead));
            moreThanOne = true;
        }

        if (theSame) {
            return listOfForces.get(0).trim();
        } else {

            // excel cell limitation is 32,767 characters
            if (listOfForces.toString().replaceAll("[ \\[\\]]", "").length() >= EXCELL_CELL_CHARACTER_LIMITATION_32_767) {
                String substring = listOfForces.toString().replaceAll("[ \\[\\]]", "").substring(0, EXCELL_CELL_CHARACTER_LIMITATION_32_767);
                return EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO + "," + substring.replaceAll(",[0-9\\\\.]*$", "");
            }

            return listOfForces.toString().replaceAll("[ \\[\\]]", "");
        }
    }

    public static String getSPCAddComponents(Loadcol currentLoadcol) {
        if (currentLoadcol.getNastranTable().size() <= 1) {
            return "";
        }

        StringBuffer result = new StringBuffer();
        List<String> listOfForces = new ArrayList<>();

        for (int i = 2; i < currentLoadcol.getNastranTable().size(); i++) {
            result.append(currentLoadcol.getNastranTable().get(i).trim() + ",");
        }
        result.replace(result.length() - 1, result.length(), "");
        return result.toString();
    }


}
