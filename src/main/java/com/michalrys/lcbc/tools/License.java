package com.michalrys.lcbc.tools;

import com.michalrys.lcbc.AppRun;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;

public class License {
    public static boolean isValid() {
        LocalDate now = LocalDate.now();
        if(now.isAfter(LocalDate.of(2020, 6, 1))) {
            System.out.println("License is out - ask Michal R.");
            AppRun.updateStatus("License is out - ask Michal R.");
            printMessageWindow();
            return false;
        }
        return true;
    }

    private static void printMessageWindow() {
        JFrame jFrame = new JFrame("License check");
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setLocation(600, 100);
        jFrame.setSize(250, 90);

        JTextArea jTextArea = new JTextArea("" +
                "    License is out. Please ask Michał R.");

        jTextArea.setEditable(false);
        jTextArea.setFont(new Font("Arial", Font.PLAIN, 12));
        jFrame.getContentPane().add(jTextArea, BorderLayout.CENTER);
        jFrame.setVisible(true);
        jFrame.setAlwaysOnTop(true);
    }
}
