package com.michalrys.lcbc.excel;

import com.michalrys.lcbc.AppRun;
import com.michalrys.lcbc.femfile.AnyFemFile;
import com.michalrys.lcbc.femfile.FemFile;
import com.michalrys.lcbc.loadcase.Loadcase;
import com.michalrys.lcbc.loadcols.LoadType;
import com.michalrys.lcbc.loadcols.Loadcol;
import com.michalrys.lcbc.tools.LoadColTools;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import java.awt.Color;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ExcelFile {
    private static FemFile femFile;

    private static final String EXCEL_EXTENSION = ".xlsx";
    private final String filePath;
    private final Workbook workbook;
    private final Sheet sheetConfig;
    private final Sheet sheetForces;
    private final Sheet sheetMultipliedForces;

    private CellStyle cellStyleMainHeaderLC;
    private CellStyle cellStyleMainHeaderGrav;
    private CellStyle cellStyleMainHeaderForce;
    private CellStyle cellStyleMainHeaderMoment;
    private CellStyle cellStyleMainHeaderPload4;
    private CellStyle cellStyleMainHeaderSPC;
    private CellStyle cellStyleMainHeaderSPCADD1;
    private CellStyle cellStyleMainHeaderLCdetails;
    private CellStyle cellStyleMainHeaderDefault;
    private CellStyle cellStyleMainHeaderBC;
    private CellStyle cellStyleMainHeaderSPCADD;
    private CellStyle cellStyleDataNormalA;
    private CellStyle cellStyleDataNormalB;
    private CellStyle cellStyleDataNormalALoadCaseName;
    private CellStyle cellStyleDataNormalBLoadCaseName;
    private CellStyle cellStyleUpperHeader;
    private CellStyle cellStyleUpperHeaderExcellError;
    private CellStyle cellStyleUpperHeaderDescription;
    private CellStyle cellStyleConfigHeader;
    private CellStyle cellStyleConfigNormal;

    private List<Loadcol> listOfGrav;
    private List<Loadcol> listOfForces;
    private List<Loadcol> listOfMoments;
    private List<Loadcol> listOfPload4s;
    private List<Loadcol> listOfSpcs;
    private List<Loadcol> listOfSpcAdds;
    private int amountOfColumns;
    private List<Loadcol> listOfForcesUnique = new ArrayList<>();
    private List<Loadcol> listOfMomentsUnique = new ArrayList<>();
    private List<Loadcol> listOfPload4sUnique = new ArrayList<>();
    private List<Loadcol> listOfSpcsUnique = new ArrayList<>();
    private List<Loadcol> listOfSpcAddsUnique = new ArrayList<>();
    private List<Loadcase> listOfLoadcases = new ArrayList<>();
    private List<Loadcol> listOfLoads = new ArrayList<>();

    public ExcelFile(String filePath) {
        this.filePath = convertToExcelFilePath(filePath);

        workbook = new XSSFWorkbook();
        sheetForces = workbook.createSheet("Forces");
        sheetMultipliedForces = workbook.createSheet("MultipliedForces");
        sheetConfig = workbook.createSheet("Config");
    }

    public void setUpWorkbook() {
        setApplicationStatus("LC/BC: initializing xlsx file...");

        XSSFFont fontNormal = (XSSFFont) workbook.createFont();
        fontNormal.setFontName("Calibri");
        fontNormal.setFontHeightInPoints((short) 11);

        XSSFFont fontGray = (XSSFFont) workbook.createFont();
        fontGray.setFontName("Calibri");
        fontGray.setColor(IndexedColors.GREY_50_PERCENT.getIndex());
        fontGray.setFontHeightInPoints((short) 11);

        XSSFFont fontBold = (XSSFFont) workbook.createFont();
        fontBold.setFontName("Calibri");
        fontBold.setFontHeightInPoints((short) 11);
        fontBold.setBold(true);

        // setup styles
        this.cellStyleMainHeaderLCdetails = this.workbook.createCellStyle();
        cellStyleMainHeaderLCdetails.setFont(fontBold);
        byte[] colorMainHeaderLCdetailsRgb = {(byte) 212, (byte) 218, (byte) 255};
        Color colorMainHeaderLCdetailsColor = new Color(212, 218, 255);
        ((XSSFCellStyle) cellStyleMainHeaderLCdetails).setFillForegroundColor(new XSSFColor(colorMainHeaderLCdetailsRgb, new DefaultIndexedColorMap()));
        //cellStyleMainHeaderLCdetails.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        cellStyleMainHeaderLCdetails.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderLCdetails.setAlignment(HorizontalAlignment.CENTER);
        cellStyleMainHeaderLCdetails.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderLCdetails.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderLCdetails.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderLCdetails.setBorderRight(BorderStyle.valueOf((short) 2));

        this.cellStyleMainHeaderLC = this.workbook.createCellStyle();
        cellStyleMainHeaderLC.setFont(fontBold);
        byte[] colorMainHeaderLCRgb = {(byte) 142, (byte) 145, (byte) 242};
        Color colorMainHeaderLCColor = new Color(142, 145, 242);
        ((XSSFCellStyle) cellStyleMainHeaderLC).setFillForegroundColor(new XSSFColor(colorMainHeaderLCRgb, new DefaultIndexedColorMap()));
        //cellStyleMainHeaderLC.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
        cellStyleMainHeaderLC.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderLC.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderLC.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderLC.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderLC.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderLC.setBorderRight(BorderStyle.valueOf((short) 2));

        this.cellStyleMainHeaderGrav = this.workbook.createCellStyle();
        cellStyleMainHeaderGrav.setFont(fontBold);
        byte[] colorHeaderGravRgb = {(byte) 75, (byte) 205, (byte) 72};
        Color colorHeaderGravColor = new Color(75, 209, 72);
        ((XSSFCellStyle) cellStyleMainHeaderGrav).setFillForegroundColor(
                new XSSFColor(colorHeaderGravRgb, new DefaultIndexedColorMap()));
        cellStyleMainHeaderGrav.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderGrav.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderGrav.setRotation((short) 90);
        cellStyleMainHeaderGrav.setWrapText(true);
        cellStyleMainHeaderGrav.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderGrav.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderGrav.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderGrav.setBorderRight(BorderStyle.valueOf((short) 2));

        this.cellStyleMainHeaderForce = this.workbook.createCellStyle();
        cellStyleMainHeaderForce.setFont(fontBold);
        byte[] colorHeaderForceRgb = {(byte) 10, (byte) 157, (byte) 0};
        Color colorHeaderForceColor = new Color(10, 157, 0);
        ((XSSFCellStyle) cellStyleMainHeaderForce).setFillForegroundColor(
                new XSSFColor(colorHeaderForceRgb, new DefaultIndexedColorMap()));
        cellStyleMainHeaderForce.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderForce.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderForce.setRotation((short) 90);
        cellStyleMainHeaderForce.setWrapText(true);
        cellStyleMainHeaderForce.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderForce.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderForce.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderForce.setBorderRight(BorderStyle.valueOf((short) 2));

        //cellStyleMainHeaderMoment;
        this.cellStyleMainHeaderMoment = this.workbook.createCellStyle();
        cellStyleMainHeaderMoment.setFont(fontBold);
        byte[] colorHeaderMomentRgb = {(byte) 9, (byte) 95, (byte) 0};
        Color colorHeaderMomentColor = new Color(9, 95, 0);
        ((XSSFCellStyle) cellStyleMainHeaderMoment).setFillForegroundColor(
                new XSSFColor(colorHeaderMomentRgb, new DefaultIndexedColorMap()));
        cellStyleMainHeaderMoment.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderMoment.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderMoment.setRotation((short) 90);
        cellStyleMainHeaderMoment.setWrapText(true);
        cellStyleMainHeaderMoment.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderMoment.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderMoment.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderMoment.setBorderRight(BorderStyle.valueOf((short) 2));

        //cellStyleMainHeaderPload4;
        this.cellStyleMainHeaderPload4 = this.workbook.createCellStyle();
        cellStyleMainHeaderPload4.setFont(fontBold);
        byte[] colorHeaderPload4Rgb = {(byte) 213, (byte) 138, (byte) 0};
        Color colorHeaderPload4Color = new Color(213, 138, 0);
        ((XSSFCellStyle) cellStyleMainHeaderPload4).setFillForegroundColor(
                new XSSFColor(colorHeaderPload4Rgb, new DefaultIndexedColorMap()));
        cellStyleMainHeaderPload4.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderPload4.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderPload4.setRotation((short) 90);
        cellStyleMainHeaderPload4.setWrapText(true);
        cellStyleMainHeaderPload4.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderPload4.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderPload4.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderPload4.setBorderRight(BorderStyle.valueOf((short) 2));

        //cellStyleMainHeaderSPC;
        this.cellStyleMainHeaderSPC = this.workbook.createCellStyle();
        cellStyleMainHeaderSPC.setFont(fontBold);
        byte[] colorHeaderSPCRgb = {(byte) 255, (byte) 146, (byte) 230};
        Color colorHeaderSPCColor = new Color(255, 146, 230);
        ((XSSFCellStyle) cellStyleMainHeaderSPC).setFillForegroundColor(
                new XSSFColor(colorHeaderSPCRgb, new DefaultIndexedColorMap()));
        cellStyleMainHeaderSPC.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderSPC.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderSPC.setRotation((short) 90);
        cellStyleMainHeaderSPC.setWrapText(true);
        cellStyleMainHeaderSPC.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPC.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPC.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPC.setBorderRight(BorderStyle.valueOf((short) 2));

        //cellStyleMainHeaderSPCADD1;
        this.cellStyleMainHeaderSPCADD1 = this.workbook.createCellStyle();
        cellStyleMainHeaderSPCADD1.setFont(fontBold);
        byte[] colorHeaderSPCADDRgb = {(byte) 176, (byte) 101, (byte) 158};
        Color colorHeaderSPCADDcolor = new Color(176, 101, 158);
        ((XSSFCellStyle) cellStyleMainHeaderSPCADD1).setFillForegroundColor(
                new XSSFColor(colorHeaderSPCADDRgb, new DefaultIndexedColorMap()));
        cellStyleMainHeaderSPCADD1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderSPCADD1.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderSPCADD1.setRotation((short) 90);
        cellStyleMainHeaderSPCADD1.setWrapText(true);
        cellStyleMainHeaderSPCADD1.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPCADD1.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPCADD1.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPCADD1.setBorderRight(BorderStyle.valueOf((short) 2));


        this.cellStyleMainHeaderDefault = this.workbook.createCellStyle();
        cellStyleMainHeaderDefault.setFont(fontBold);
        cellStyleMainHeaderDefault.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        cellStyleMainHeaderDefault.setVerticalAlignment(VerticalAlignment.BOTTOM);
        cellStyleMainHeaderDefault.setAlignment(HorizontalAlignment.LEFT);
        cellStyleMainHeaderDefault.setRotation((short) 90);
        cellStyleMainHeaderDefault.setWrapText(true);
        cellStyleMainHeaderDefault.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderDefault.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderDefault.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderDefault.setBorderTop(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderDefault.setBorderRight(BorderStyle.valueOf((short) 2));

        this.cellStyleMainHeaderBC = this.workbook.createCellStyle();
        cellStyleMainHeaderBC.setFont(fontBold);
        cellStyleMainHeaderBC.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        cellStyleMainHeaderBC.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderBC.setAlignment(HorizontalAlignment.CENTER);
        cellStyleMainHeaderBC.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderBC.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderBC.setBorderRight(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderBC.setBorderTop(BorderStyle.valueOf((short) 2));

        this.cellStyleMainHeaderSPCADD = this.workbook.createCellStyle();
        cellStyleMainHeaderSPCADD.setFont(fontBold);
        cellStyleMainHeaderSPCADD.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleMainHeaderSPCADD.setFillForegroundColor(IndexedColors.RED.getIndex());
        cellStyleMainHeaderSPCADD.setAlignment(HorizontalAlignment.CENTER);
        cellStyleMainHeaderSPCADD.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPCADD.setBorderRight(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPCADD.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleMainHeaderSPCADD.setBorderTop(BorderStyle.valueOf((short) 2));

        this.cellStyleDataNormalA = this.workbook.createCellStyle();
        cellStyleDataNormalA.setFont(fontNormal);
        cellStyleDataNormalA.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleDataNormalA.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleDataNormalA.setAlignment(HorizontalAlignment.CENTER);
        cellStyleDataNormalA.setBorderBottom(BorderStyle.THIN);
        cellStyleDataNormalA.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalA.setBorderRight(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalA.setBorderTop(BorderStyle.THIN);

        this.cellStyleDataNormalALoadCaseName = this.workbook.createCellStyle();
        cellStyleDataNormalALoadCaseName.setFont(fontNormal);
        cellStyleDataNormalALoadCaseName.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleDataNormalALoadCaseName.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleDataNormalALoadCaseName.setAlignment(HorizontalAlignment.LEFT);
        cellStyleDataNormalALoadCaseName.setBorderBottom(BorderStyle.THIN);
        cellStyleDataNormalALoadCaseName.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalALoadCaseName.setBorderRight(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalALoadCaseName.setBorderTop(BorderStyle.THIN);

        this.cellStyleDataNormalB = this.workbook.createCellStyle();
        cellStyleDataNormalB.setFont(fontNormal);
        cellStyleDataNormalB.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyleDataNormalB.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleDataNormalB.setAlignment(HorizontalAlignment.CENTER);
        cellStyleDataNormalB.setBorderBottom(BorderStyle.THIN);
        cellStyleDataNormalB.setBorderRight(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalB.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalB.setBorderTop(BorderStyle.THIN);

        this.cellStyleDataNormalBLoadCaseName = this.workbook.createCellStyle();
        cellStyleDataNormalBLoadCaseName.setFont(fontNormal);
        cellStyleDataNormalBLoadCaseName.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyleDataNormalBLoadCaseName.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleDataNormalBLoadCaseName.setAlignment(HorizontalAlignment.LEFT);
        cellStyleDataNormalBLoadCaseName.setBorderBottom(BorderStyle.THIN);
        cellStyleDataNormalBLoadCaseName.setBorderRight(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalBLoadCaseName.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleDataNormalBLoadCaseName.setBorderTop(BorderStyle.THIN);

        this.cellStyleUpperHeader = this.workbook.createCellStyle();
        cellStyleUpperHeader.setFont(fontGray);
        cellStyleUpperHeader.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
        cellStyleUpperHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleUpperHeader.setAlignment(HorizontalAlignment.LEFT);
        cellStyleUpperHeader.setBorderTop(BorderStyle.THIN);
        cellStyleUpperHeader.setBorderLeft(BorderStyle.THIN);
        cellStyleUpperHeader.setBorderRight(BorderStyle.THIN);

        this.cellStyleUpperHeaderDescription = this.workbook.createCellStyle();
        cellStyleUpperHeaderDescription.setFont(fontGray);
//        byte[] colorByteCellStyleUpperHeaderDescription = {(byte) 255, (byte) 255, (byte) 255};
//        Color colorCellStyleUpperHeaderDescription = new Color(255, 255, 255);
//        ((XSSFCellStyle) cellStyleUpperHeaderDescription).setFillForegroundColor(
//                new XSSFColor(colorByteCellStyleUpperHeaderDescription, new DefaultIndexedColorMap()));
//        cellStyleUpperHeaderDescription.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleUpperHeaderDescription.setAlignment(HorizontalAlignment.RIGHT);

        this.cellStyleUpperHeaderExcellError = this.workbook.createCellStyle();
        cellStyleUpperHeaderExcellError.setFont(fontGray);
        byte[] CellStyleUpperHeaderExcellError = {(byte) 255, (byte) 134, (byte) 112};
        Color colorCellStyleUpperHeaderExcellError = new Color(255, 134, 112);
        ((XSSFCellStyle) cellStyleUpperHeaderExcellError).setFillForegroundColor(
                new XSSFColor(CellStyleUpperHeaderExcellError, new DefaultIndexedColorMap()));
        cellStyleUpperHeaderExcellError.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleUpperHeaderExcellError.setAlignment(HorizontalAlignment.LEFT);
        cellStyleUpperHeaderExcellError.setBorderTop(BorderStyle.THIN);
        cellStyleUpperHeaderExcellError.setBorderLeft(BorderStyle.THIN);
        cellStyleUpperHeaderExcellError.setBorderRight(BorderStyle.THIN);

        this.cellStyleConfigHeader = this.workbook.createCellStyle();
        cellStyleConfigHeader.setFont(fontBold);
        cellStyleConfigHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyleConfigHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleConfigHeader.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleConfigHeader.setBorderRight(BorderStyle.valueOf((short) 2));
        cellStyleConfigHeader.setBorderBottom(BorderStyle.valueOf((short) 2));
        cellStyleConfigHeader.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleConfigHeader.setBorderTop(BorderStyle.valueOf((short) 2));

        this.cellStyleConfigNormal = this.workbook.createCellStyle();
        cellStyleConfigNormal.setFont(fontNormal);
        cellStyleConfigNormal.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyleConfigNormal.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleConfigNormal.setAlignment(HorizontalAlignment.LEFT);
        cellStyleConfigNormal.setBorderBottom(BorderStyle.THIN);
        cellStyleConfigNormal.setBorderLeft(BorderStyle.valueOf((short) 2));
        cellStyleConfigNormal.setBorderTop(BorderStyle.THIN);
        cellStyleConfigNormal.setBorderRight(BorderStyle.THIN);

        sheetConfig.setColumnWidth(0, 10000);
        sheetConfig.setColumnWidth(1, 5000);

        Row row;
        Cell cell;
        row = sheetConfig.createRow(0);
        cell = row.createCell(0);
        cell.setCellStyle(cellStyleConfigHeader);
        cell.setCellValue("Auto repair spcadd");
        cell = row.createCell(1);
        cell.setCellStyle(cellStyleConfigNormal);
        cell.setCellValue(false);

        row = sheetConfig.createRow(1);
        cell = row.createCell(0);
        cell.setCellStyle(cellStyleConfigHeader);
        cell.setCellValue("Auto repair LOAD id");
        cell = row.createCell(1);
        cell.setCellStyle(cellStyleConfigNormal);
        cell.setCellValue(false);
    }

    public void fillCellsWith(FemFile femFile) {
        this.femFile = femFile;
        extractDataFromFemFile();
        Row row;
        Cell cell;

        sheetForces.setColumnWidth(0, 800); // empty column
        sheetForces.setColumnWidth(1, 2000); // SPC id
        sheetForces.setColumnWidth(2, 2000); // LOAD global factor
        sheetForces.setColumnWidth(3, 2000); // LOAD id
        sheetForces.setColumnWidth(4, 2000); // SUBCASE id
        sheetForces.setColumnWidth(5, 10000); // SUBCASE name

        sheetForces.setColumnWidth(6, 2500); // force 1
        sheetForces.setColumnWidth(7, 2500); // force 2
        sheetForces.setColumnWidth(8, 2500); // force 3
        sheetForces.setColumnWidth(9, 2500); // force ...

        sheetForces.setColumnWidth(10, 2500); // bc1
        sheetForces.setColumnWidth(11, 2500); // bc2
        sheetForces.setColumnWidth(12, 2500); // bc3 ...

        sheetForces.setColumnWidth(13, 2500); // SPCADD

        int rowStartingPosition = 11;
        int cellStartingPosition = 1;
        int currentRow = rowStartingPosition; //starting position
        int currentCell = cellStartingPosition; //starting position
        int additionalOffsetForLocationOfLoadcolsFromLeft = 5;

        // set upper header style
        for (int j = 0; j <= 10; j++) {
            row = sheetForces.createRow(j);
            for (int i = 6; i <= amountOfColumns + additionalOffsetForLocationOfLoadcolsFromLeft; i++) {
                cell = row.createCell(i);
                cell.setCellStyle(cellStyleUpperHeader);
            }
        }
        // set headers = row = 11
        row = sheetForces.createRow(currentRow);
        row.setHeight((short) 1500);
        for (int i = 1; i <= amountOfColumns + additionalOffsetForLocationOfLoadcolsFromLeft; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleMainHeaderDefault);
        }

        row = sheetForces.getRow(currentRow);
        cell = row.getCell(currentCell);
        cell.setCellStyle(cellStyleMainHeaderLCdetails);
        cell.setCellValue("SPC");

        row = sheetForces.getRow(currentRow);
        cell = row.getCell(++currentCell);
        cell.setCellStyle(cellStyleMainHeaderLCdetails);
        cell.setCellValue("GLF");

        row = sheetForces.getRow(currentRow);
        cell = row.getCell(++currentCell);
        cell.setCellStyle(cellStyleMainHeaderLCdetails);
        cell.setCellValue("LOAD");

        row = sheetForces.getRow(currentRow);
        cell = row.getCell(++currentCell);
        cell.setCellStyle(cellStyleMainHeaderLCdetails);
        cell.setCellValue("LC");

        row = sheetForces.getRow(currentRow);
        cell = row.getCell(++currentCell);
        cell.setCellStyle(cellStyleMainHeaderLC);
        cell.setCellValue("LOADCASE");

        setApplicationStatus("LC/BC: initializing xlsx file - done.");
        //--------------------------------------------------------------------------------------------------------------
        // set cells description
        int cellDescription = currentCell;
        int rowDesciption = currentRow;
        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("card: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("id: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("CID: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("other: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("factor: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("x: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("y: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("z: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("hm color: ");

        row = sheetForces.getRow(--rowDesciption);
        cell = row.createCell(cellDescription);
        cell.setCellStyle(cellStyleUpperHeaderDescription);
        cell.setCellValue("node: ");

        //--------------------------------------------------------------------------------------------------------------
        // set gravs:
        setApplicationStatus("LC/BC: extracting GRAVs...");
        int currentCellGrav = currentCell;
        int currentRowGrav = currentRow;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(grav.getDescription());
            cell.setCellStyle(cellStyleMainHeaderGrav);
        }

        // set grav CARD:
        currentRowGrav--;
        currentCellGrav = currentCell;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(grav.getLoadType().toString());
        }

        // set gravs ids:
        currentRowGrav--;
        currentCellGrav = currentCell;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(grav.getId());
        }

        currentRowGrav -= 2;
        // set gravs value:
        currentRowGrav--;
        currentCellGrav = currentCell;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(Double.parseDouble(grav.getNastranTable().get(3)));
        }
        // set gravs X:
        currentRowGrav--;
        currentCellGrav = currentCell;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(Double.parseDouble(grav.getNastranTable().get(4)));
        }
        // set gravs Y:
        currentRowGrav--;
        currentCellGrav = currentCell;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(Double.parseDouble(grav.getNastranTable().get(5)));
        }
        // set gravs Z:
        currentRowGrav--;
        currentCellGrav = currentCell;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(Double.parseDouble(grav.getNastranTable().get(6)));
        }
        // set gravs color:
        currentRowGrav--;
        currentCellGrav = currentCell;
        for (Loadcol grav : listOfGrav) {
            row = sheetForces.getRow(currentRowGrav);
            cell = row.getCell(++currentCellGrav);
            cell.setCellValue(grav.getColorId());
        }
        setApplicationStatus("LC/BC: extracting GRAVs - done.");
        //--------------------------------------------------------------------------------------------------------------
        // set forces
        setApplicationStatus("LC/BC: extracting FORCEs...");
        currentRow = currentRow;
        currentCell = currentCellGrav;
        int currentCellForce = currentCell;
        int currentRowForce = currentRow;

        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(force.getDescription());
            cell.setCellStyle(cellStyleMainHeaderForce);
        }

        // set force CARD
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(force.getLoadType().toString());
        }

        //set ids
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(force.getId());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set CID
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            //cell.setCellValue(Double.parseDouble(force.getNastranTable().get(3)));
            cell.setCellValue(LoadColTools.getForceComponents(force, "CID", listOfForces).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        currentRowForce -= 1;
        //set factor
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            //cell.setCellValue(Double.parseDouble(force.getNastranTable().get(4)));
            cell.setCellValue(LoadColTools.getForceComponents(force, "factor", listOfForces).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set x value
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(LoadColTools.getForceComponents(force, "Fx", listOfForces).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set y value
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(LoadColTools.getForceComponents(force, "Fy", listOfForces).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set z value
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(LoadColTools.getForceComponents(force, "Fz", listOfForces).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set color
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(force.getColorId());
        }

        // set nodes
        currentRowForce--;
        currentCellForce = currentCell;
        for (Loadcol force : listOfForcesUnique) {
            row = sheetForces.getRow(currentRowForce);
            cell = row.getCell(++currentCellForce);
            cell.setCellValue(LoadColTools.getNodes(force, listOfForces).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }
        setApplicationStatus("LC/BC: extracting FORCEs - done.");
        //--------------------------------------------------------------------------------------------------------------
        // set moments
        setApplicationStatus("LC/BC: extracting MOMENTs...");
        currentRow = currentRow;
        currentCell = currentCellForce;
        int currentRowMoment = currentRow;
        int currentCellMoment = currentCell;

        for (Loadcol moment : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(moment.getDescription());
            cell.setCellStyle(cellStyleMainHeaderMoment);
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        // set moment card
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(force.getLoadType().toString());
        }

        //set ids
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(force.getId());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set CID
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
//            cell.setCellValue(Double.parseDouble(force.getNastranTable().get(3)));
            cell.setCellValue(LoadColTools.getMomentComponents(force, "CID", listOfMoments).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        currentRowMoment -= 1;

        //set factor
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            //cell.setCellValue(Double.parseDouble(force.getNastranTable().get(4)));
            cell.setCellValue(LoadColTools.getMomentComponents(force, "factor", listOfMoments).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set x value
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(LoadColTools.getMomentComponents(force, "Mx", listOfMoments).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set y value
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(LoadColTools.getMomentComponents(force, "My", listOfMoments).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set z value
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(LoadColTools.getMomentComponents(force, "Mz", listOfMoments).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set color
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(force.getColorId());
        }

        // set nodes
        currentRowMoment--;
        currentCellMoment = currentCell;
        for (Loadcol force : listOfMomentsUnique) {
            row = sheetForces.getRow(currentRowMoment);
            cell = row.getCell(++currentCellMoment);
            cell.setCellValue(LoadColTools.getNodes(force, listOfMoments));
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }
        setApplicationStatus("LC/BC: extracting MOMENTs - done.");
        //--------------------------------------------------------------------------------------------------------------
        // set pload4
        setApplicationStatus("LC/BC: extracting PLOAD4s...");
        currentRow = currentRow;
        currentCell = currentCellMoment;

        int currentRowPload4 = currentRow;
        int currentCellPload4 = currentCell;

        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(loadcol.getDescription());
            cell.setCellStyle(cellStyleMainHeaderPload4);
        }

        // set force CARD
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(loadcol.getLoadType().toString());
        }

        //set ids
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(loadcol.getId());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set CID
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(LoadColTools.getPload4sComponents(loadcol, "CID", listOfPload4s).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set other
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(LoadColTools.getPload4sComponents(loadcol, "Other", listOfPload4s).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set global factor
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(LoadColTools.getPload4sComponents(loadcol, "GlobalFactor", listOfPload4s).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }


        //set N1
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(LoadColTools.getPload4sComponents(loadcol, "N1", listOfPload4s));
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set N2
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(LoadColTools.getPload4sComponents(loadcol, "N2", listOfPload4s));
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set N3
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(LoadColTools.getPload4sComponents(loadcol, "N3", listOfPload4s));
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set color
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(loadcol.getColorId());
        }

        // set nodes
        currentRowPload4--;
        currentCellPload4 = currentCell;
        for (Loadcol loadcol : listOfPload4sUnique) {
            row = sheetForces.getRow(currentRowPload4);
            cell = row.getCell(++currentCellPload4);
            cell.setCellValue(LoadColTools.getNodes(loadcol, listOfPload4s).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }
        setApplicationStatus("LC/BC: extracting PLOAD4s - done.");
        //--------------------------------------------------------------------------------------------------------------
        // set spc
        setApplicationStatus("LC/BC: extracting SPCs...");
        currentRow = currentRow;
        currentCell = currentCellPload4;

        int currentRowSPC = currentRow;
        int currentCellSPC = currentCell;

        for (Loadcol spc : listOfSpcsUnique) {
            row = sheetForces.getRow(currentRowSPC);
            cell = row.getCell(++currentCellSPC);
            cell.setCellValue(spc.getDescription());
            cell.setCellStyle(cellStyleMainHeaderSPC);
        }

        // set force CARD
        currentRowSPC--;
        currentCellSPC = currentCell;
        for (Loadcol loadcol : listOfSpcsUnique) {
            row = sheetForces.getRow(currentRowSPC);
            cell = row.getCell(++currentCellSPC);
            cell.setCellValue(loadcol.getLoadType().toString());
        }

        //set ids
        currentRowSPC--;
        currentCellSPC = currentCell;
        for (Loadcol loadcol : listOfSpcsUnique) {
            row = sheetForces.getRow(currentRowSPC);
            cell = row.getCell(++currentCellSPC);
            cell.setCellValue(loadcol.getId());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        currentRowSPC -= 1;

        //set direction of fixing
        currentRowSPC--;
        currentCellSPC = currentCell;
        for (Loadcol loadcol : listOfSpcsUnique) {
            row = sheetForces.getRow(currentRowSPC);
            cell = row.getCell(++currentCellSPC);
            cell.setCellValue(LoadColTools.getSPCComponents(loadcol, "Direction", listOfSpcs).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        //set fixing value
        currentRowSPC--;
        currentCellSPC = currentCell;
        for (Loadcol loadcol : listOfSpcsUnique) {
            row = sheetForces.getRow(currentRowSPC);
            cell = row.getCell(++currentCellSPC);
            cell.setCellValue(LoadColTools.getSPCComponents(loadcol, "Value", listOfSpcs).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        currentRowSPC -= 3;

        //set color
        currentRowSPC--;
        currentCellSPC = currentCell;
        for (Loadcol loadcol : listOfSpcsUnique) {
            row = sheetForces.getRow(currentRowSPC);
            cell = row.getCell(++currentCellSPC);
            cell.setCellValue(loadcol.getColorId());
        }

        // set nodes
        currentRowSPC--;
        currentCellSPC = currentCell;
        for (Loadcol loadcol : listOfSpcsUnique) {
            row = sheetForces.getRow(currentRowSPC);
            cell = row.getCell(++currentCellSPC);
            cell.setCellValue(LoadColTools.getNodes(loadcol, listOfSpcs).toString());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }
        setApplicationStatus("LC/BC: extracting SPCs - done.");
        //--------------------------------------------------------------------------------------------------------------
        // set spcAdd
        setApplicationStatus("LC/BC: extracting SPCADDs...");
        currentRow = currentRow;
        currentCell = currentCellSPC;

        int currentRowSPCAdd = currentRow;
        int currentCellSPCAdd = currentCell;

        for (Loadcol spcAdd : listOfSpcAdds) {
            row = sheetForces.getRow(currentRowSPCAdd);
            cell = row.getCell(++currentCellSPCAdd);
            cell.setCellValue(spcAdd.getDescription());
            cell.setCellStyle(cellStyleMainHeaderSPCADD1);
        }

        // set force CARD
        currentRowSPCAdd--;
        currentCellSPCAdd = currentCell;
        for (Loadcol loadcol : listOfSpcAddsUnique) {
            row = sheetForces.getRow(currentRowSPCAdd);
            cell = row.getCell(++currentCellSPCAdd);
            cell.setCellValue(loadcol.getLoadType().toString());
        }

        //set ids
        currentRowSPCAdd--;
        currentCellSPCAdd = currentCell;
        for (Loadcol loadcol : listOfSpcAddsUnique) {
            row = sheetForces.getRow(currentRowSPCAdd);
            cell = row.getCell(++currentCellSPCAdd);
            cell.setCellValue(loadcol.getId());
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        currentRowSPCAdd -= 1;

        //set fixing value
        currentRowSPCAdd--;
        currentCellSPCAdd = currentCell;
        for (Loadcol loadcol : listOfSpcAddsUnique) {
            row = sheetForces.getRow(currentRowSPCAdd);
            cell = row.getCell(++currentCellSPCAdd);
            cell.setCellValue(LoadColTools.getSPCAddComponents(loadcol));
            checkIfExcellCellCharacterLimitationIsReached(cell);
        }

        currentRowSPCAdd -= 4;

        //set color
        currentRowSPCAdd--;
        currentCellSPCAdd = currentCell;
        for (Loadcol loadcol : listOfSpcAddsUnique) {
            row = sheetForces.getRow(currentRowSPCAdd);
            cell = row.getCell(++currentCellSPCAdd);
            cell.setCellValue(loadcol.getColorId());
        }

        currentRow = currentRow;
        currentCell = currentCellSPCAdd;
        setApplicationStatus("LC/BC: extracting SPCADDs - done.");
        //--------------------------------------------------------------------------------------------------------------
        //--- loadcases ------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        // SPC | LOAD | LC | LOADCASE
        setApplicationStatus("LC/BC: extracting LOADCASEs...");
        currentRow = rowStartingPosition + 1;
        currentCell = cellStartingPosition;

        for (int i = 0; i < listOfLoadcases.size(); i++) {
            // spc
            row = sheetForces.createRow(currentRow + i);
            cell = row.createCell(currentCell + 0);
            cell.setCellValue(listOfLoadcases.get(i).getSpcId());
            if ((currentRow + i) % 2 == 0) {
                cell.setCellStyle(cellStyleDataNormalA);
            } else {
                cell.setCellStyle(cellStyleDataNormalB);
            }

            // global load factor
            row = sheetForces.getRow(currentRow + i);
            cell = row.createCell(currentCell + 1);

            Integer loadId = listOfLoadcases.get(i).getLoadId();
            double globalFactor = 1.0;
            for (Loadcol load : listOfLoads) {
                if (load.getId() == loadId) {
                    globalFactor = Double.parseDouble(load.getNastranTable().get(2).trim());
                }
                break;
            }
            cell.setCellValue(globalFactor);
            if ((currentRow + i) % 2 == 0) {
                cell.setCellStyle(cellStyleDataNormalA);
            } else {
                cell.setCellStyle(cellStyleDataNormalB);
            }

            // load
            row = sheetForces.getRow(currentRow + i);
            cell = row.createCell(currentCell + 2);
            cell.setCellValue(listOfLoadcases.get(i).getLoadId());
            if ((currentRow + i) % 2 == 0) {
                cell.setCellStyle(cellStyleDataNormalA);
            } else {
                cell.setCellStyle(cellStyleDataNormalB);
            }

            // lc
            row = sheetForces.getRow(currentRow + i);
            cell = row.createCell(currentCell + 3);
            cell.setCellValue(listOfLoadcases.get(i).getId());
            if ((currentRow + i) % 2 == 0) {
                cell.setCellStyle(cellStyleDataNormalA);
            } else {
                cell.setCellStyle(cellStyleDataNormalB);
            }

            // loadcase
            row = sheetForces.getRow(currentRow + i);
            cell = row.createCell(currentCell + 4);
            cell.setCellValue(listOfLoadcases.get(i).getHmDescription());
            if ((currentRow + i) % 2 == 0) {
                cell.setCellStyle(cellStyleDataNormalALoadCaseName);
            } else {
                cell.setCellStyle(cellStyleDataNormalBLoadCaseName);
            }
        }
        setApplicationStatus("LC/BC: extracting LOADCASEs - done.");
        //--------------------------------------------------------------------------------------------------------------
        // LOADS |
        setApplicationStatus("LC/BC: extracting LOADs...");
        currentRow = rowStartingPosition + 1;
        currentCell = cellStartingPosition + additionalOffsetForLocationOfLoadcolsFromLeft;
        int cellWhereSPCIs = currentCell;

        // here listOfLoads will be reduced to zero
        //System.out.println(listOfLoads.toString());

        Loadcol currentLoad = listOfLoads.get(0);

        for (int i = 0; i < listOfLoadcases.size(); i++) {
            row = sheetForces.getRow(currentRow + i);

            Integer loadId = listOfLoadcases.get(i).getLoadId();
            for (Loadcol load : listOfLoads) {
                if (load.getId().equals(loadId)) {
                    currentLoad = load;
                    listOfLoads.remove(currentLoad);
                    break;
                }
            }

            for (int j = currentCell; j < (currentCell + amountOfColumns); j++) {
                cell = row.createCell(j);
                //cell.setCellValue("HELLO");

                // check if we are in spc columns
                Row rowWithCard = sheetForces.getRow(currentRow - 2);
                if (rowWithCard.getCell(j).getStringCellValue().equals("SPC")) {
                    cellWhereSPCIs = j;
                    break;
                }

                if (currentLoad.getNastranTable().size() >= 4) {

                    boolean skipEven = false;
                    for (int id = 4; id < currentLoad.getNastranTable().size(); id++) {
                        // this is tricky check - because load could be continues, then there is additional cell in
                        // in nastran table. Therefore, we must move.
                        if (id % 9 == 0) {
                            skipEven = !skipEven;
                            continue;
                        }
                        if (id % 2 == 0 && skipEven) {
                            continue;
                        }
                        if (id % 2 != 0 && !skipEven) {
                            continue;
                        }

                        int componentLoadId = Integer.parseInt(currentLoad.getNastranTable().get(id).trim());
                        Row rowForComponentLoadId = sheetForces.getRow(currentRow - 3);
                        Cell cellForComponentLoadId = rowForComponentLoadId.getCell(j);
                        int componentLoadIdInExcelList = (int) cellForComponentLoadId.getNumericCellValue();

                        if (componentLoadId == componentLoadIdInExcelList) {
                            cell.setCellValue(Double.parseDouble(currentLoad.getNastranTable().get(id - 1).trim()));
                            break;
                        } else {
                            cell.setCellValue(0);
                        }
                    }
                }


                if ((currentRow + i) % 2 == 0) {
                    cell.setCellStyle(cellStyleDataNormalA);
                } else {
                    cell.setCellStyle(cellStyleDataNormalB);
                }
            }

        }
        // listOfLoads is empty now.
        setApplicationStatus("LC/BC: extracting LOADs - done.");
        //--------------------------------------------------------------------------------------------------------------
        // | SPC
        setApplicationStatus("LC/BC: extracting SPCs...");
        currentRow = rowStartingPosition + 1;
        currentCell = cellWhereSPCIs;

//        System.out.println("currentRow " + currentRow);
//        System.out.println("currentCell " + currentCell);

        Loadcol currentSpc = listOfSpcs.get(0);

        for (int i = 0; i < listOfLoadcases.size(); i++) {
            row = sheetForces.getRow(currentRow + i);

            Integer spcId = listOfLoadcases.get(i).getSpcId();
            boolean spcIdWasFound = false;

            for (Loadcol spc : listOfSpcs) {
                if (spc.getId().equals(spcId)) {
                    currentSpc = spc;
                    spcIdWasFound = true;
                    break;
                }
            }

            if (!spcIdWasFound) {
                for (Loadcol spc : listOfSpcAdds) {
                    if (spc.getId().equals(spcId)) {
                        currentSpc = spc;
                        break;
                    }
                }
            }

            // printing
            for (int j = currentCell; j < (currentCell
                    + 1 + additionalOffsetForLocationOfLoadcolsFromLeft + amountOfColumns - cellWhereSPCIs); j++) {
                cell = row.createCell(j);
//                cell.setCellValue("HELLO");

                // check if we are in spc columns
                Row rowWithCard = sheetForces.getRow(currentRow - 2);
                if (rowWithCard.getCell(j).getStringCellValue().equals("SPCADD")) {
                    break;
                }

                if ((currentSpc.getLoadType().equals(LoadType.SPC) && currentSpc.getNastranTable().size() >= 1)) {
                    int currentSpcId = Integer.parseInt(currentSpc.getNastranTable().get(1).trim());
                    Row rowForComponentSpcId = sheetForces.getRow(currentRow - 3);
                    Cell cellForComponentSpcId = rowForComponentSpcId.getCell(j);
                    int componentSpcIdInExcelList = (int) cellForComponentSpcId.getNumericCellValue();

                    if (currentSpcId == componentSpcIdInExcelList) {
                        cell.setCellValue(1);
                    } else {
                        cell.setCellValue(0);
                    }
                }

                if ((currentSpc.getLoadType().equals(LoadType.SPCADD) && currentSpc.getNastranTable().size() >= 2)) {
                    for (int id = 2; id < currentSpc.getNastranTable().size(); id++) {
                        if (id % 9 == 0) {
                            continue;
                        }
                        int componentSpcId = Integer.parseInt(currentSpc.getNastranTable().get(id).trim());
                        Row rowForComponentSpcId = sheetForces.getRow(currentRow - 3);
                        Cell cellForComponentSpcId = rowForComponentSpcId.getCell(j);
                        int componentSpcIdInExcelList = (int) cellForComponentSpcId.getNumericCellValue();

                        if (componentSpcId == componentSpcIdInExcelList) {
                            cell.setCellValue(1);
                            break;
                        } else {
                            cell.setCellValue(0);
                        }
                    }
                }

                if ((currentRow + i) % 2 == 0) {
                    cell.setCellStyle(cellStyleDataNormalA);
                } else {
                    cell.setCellStyle(cellStyleDataNormalB);
                }
            }

        }
        setApplicationStatus("LC/BC: extracting SPCs - done.");
    }

    private void checkIfExcellCellCharacterLimitationIsReached(Cell cell) {
        // check if EXCELL_CELL_CHARACTER_LIMITATION is reached
        try {
            if (String.valueOf(cell.getNumericCellValue()).contains(LoadColTools.EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO)) {
                cell.setCellStyle(cellStyleUpperHeaderExcellError);
            }
            return;
        } catch (Exception e) {
        }

        try {
            if (cell.getStringCellValue().contains(LoadColTools.EXCELL_CELL_CHARACTER_LIMITATION_32_767_NODES_INFO)) {
                cell.setCellStyle(cellStyleUpperHeaderExcellError);
            }
            return;
        } catch (Exception e) {
        }
    }

    private void extractDataFromFemFile() {
        listOfGrav = ((AnyFemFile) femFile).getListOfGrav();
        listOfForces = ((AnyFemFile) femFile).getLoadcolForces();
        listOfMoments = ((AnyFemFile) femFile).getLoadcolMoment();
        listOfPload4s = ((AnyFemFile) femFile).getLoadcolPload4s();
        listOfSpcs = ((AnyFemFile) femFile).getLoadcolSpcs();
        listOfSpcAdds = ((AnyFemFile) femFile).getLoadcolSpcAdds();
        listOfLoads = ((AnyFemFile) femFile).getLoadcolLoads();

        listOfForcesUnique = getLoadColsUniqueList(listOfForces);
        listOfMomentsUnique = getLoadColsUniqueList(listOfMoments);
        listOfPload4sUnique = getLoadColsUniqueList(listOfPload4s);
        listOfSpcsUnique = getLoadColsUniqueList(listOfSpcs);
        listOfSpcAddsUnique = getLoadColsUniqueList(listOfSpcAdds);

        amountOfColumns = listOfGrav.size() + listOfForcesUnique.size() + listOfMomentsUnique.size() +
                listOfPload4sUnique.size() + listOfSpcsUnique.size() + listOfSpcAddsUnique.size();

        listOfLoadcases = ((AnyFemFile) femFile).getLoadcases();
    }

    private List<Loadcol> getLoadColsUniqueList(List<Loadcol> loadColList) {
        Set<String> loadColDescriptions = new TreeSet<>();
        for (Loadcol force : loadColList) {
            loadColDescriptions.add(force.getDescription());
        }
        List<Loadcol> listOfLoadColUnique = new ArrayList<>();
        for (Loadcol force : loadColList) {
            if (loadColDescriptions.contains(force.getDescription())) {
                listOfLoadColUnique.add(force);
                loadColDescriptions.remove(force.getDescription());
            }
        }
        return listOfLoadColUnique;
    }

    public void writeToFile() {
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            workbook.write(fileOutputStream);
            workbook.close();
            System.out.println("LC/BC: File was written: " + filePath);
            AppRun.updateStatus("LC/BC: File was written: " + filePath);
            AppRun.updateStatus("Finished.");
            AppRun.updateStatus("-------------------------------------");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String convertToExcelFilePath(String filePath) {
        String fileName = filePath.replaceAll(".*/", "");
        String pathOnly = filePath.replaceAll(fileName, "");
        String nameOnly = fileName.replaceAll("\\..*", "");
        String result = pathOnly + nameOnly + EXCEL_EXTENSION;
        return result;
    }

    private void setApplicationStatus(String newStatus) {
        AppRun.updateStatus(newStatus);
    }
}
