package com.michalrys.lcbc.loadcase;

import java.util.Objects;

public class Loadcase {
    private static final String $_HMNAME_LOADSTEP = "$HMNAME LOADSTEP";
    private static final String SUBCASE = "SUBCASE";
    private static final String __LABEL_ = "LABEL=";
    private static final String __SPC___ = "SPC =";
    private static final String __LOAD__ = "LOAD =";
    private String hmDescription;
    private Integer id;
    private String label;
    private Integer spcId;
    private Integer loadId;

    public Loadcase() {
    }
    public Loadcase(String readLine) {

        if (readLine.contains($_HMNAME_LOADSTEP)) {
            //$HMNAME LOADSTEP               1"LC001_Test_load1_Fx1_Fy"
            //1234567812345678123456781234567812345678123456781234567812345678
            // set id and hm description
            id = Integer.parseInt(readLine.substring(16, 32).trim());
            hmDescription = readLine.substring(32).replaceAll("\"", "");
            return;
        }
        if (readLine.contains(SUBCASE)) {
            //SUBCASE       2
            //1234567812345678
            id = Integer.parseInt(readLine.substring(8).trim());
            return;
        }
    }

    public void addContinuationData(String readLine) {
        if (readLine.contains(__LABEL_)) {
            //  LABEL= LC002_Static_test_load2
            //1234567812345678
            this.label = readLine.substring(8);
            return;
        }
        if (readLine.contains(__SPC___)) {
            //  SPC =       11
            //1234567812345678
            this.spcId = Integer.parseInt(readLine.substring(8).trim());
            return;
        }
        if (readLine.contains(__LOAD__)) {
            //  LOAD =     1002
            //1234567812345678
            this.loadId = Integer.parseInt(readLine.substring(8).trim());
            return;
        }
    }

    public void updateNullOnlyBy(Loadcase loadcase) {
        if (id == null) {
            id = loadcase.getId();
        }
        if (hmDescription == null) {
            hmDescription = loadcase.getHmDescription();
        }
        if (label == null) {
            label = loadcase.getLabel();
        }
        if (spcId == null) {
            spcId = loadcase.getSpcId();
        }
        if (loadId == null) {
            loadId = loadcase.getLoadId();
        }
    }

    public String print() {
        StringBuilder result = new StringBuilder();
        //-- start
        result.append("$\n");

        //-- 1
        result.append("$HMNAME LOADSTEP");

        int idSpaces = 16 - id.toString().length();
        result.append(emptySpaces(idSpaces) + id);

        if (hmDescription == null) {
            result.append("\"" + "loadcase_" + id + "\"" + "\n");
        } else {
            result.append("\"" + hmDescription + "\"" + "\n");
        }
        //-- 2
        result.append("  LABEL=" + label + "\n");

        //-- 3
        int spcIdSpaces = 8 - spcId.toString().length();
        result.append("  SPC = " + emptySpaces(spcIdSpaces) + spcId + "\n");

        //-- 4
        int loadIdSpaces = 8 - loadId.toString().length();
        result.append("  LOAD =" + emptySpaces(loadIdSpaces) + loadId + "\n");

        //-- end
        result.append("$\n");
        return result.toString();
    }

    String emptySpaces(int howMany) {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= howMany; i++) {
            result.append(" ");
        }
        return result.toString();
    }

    public String getHmDescription() {
        return hmDescription;
    }

    public void setHmDescription(String hmDescription) {
        this.hmDescription = hmDescription;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getSpcId() {
        return spcId;
    }

    public void setSpcId(Integer spcId) {
        this.spcId = spcId;
    }

    public Integer getLoadId() {
        return loadId;
    }

    public void setLoadId(Integer loadId) {
        this.loadId = loadId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loadcase loadcase = (Loadcase) o;
        return Objects.equals(id, loadcase.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Loadcase{" +
                "hmDescription='" + hmDescription + '\'' +
                ", id=" + id +
                ", label='" + label + '\'' +
                ", spcId=" + spcId +
                ", loadId=" + loadId +
                '}';
    }
}
