package com.michalrys.lcbc;

import com.michalrys.MVC;
import com.michalrys.lcbc.excel.ExcelFile;
import com.michalrys.lcbc.femfile.AnyFemFile;
import com.michalrys.lcbc.femfile.FemFile;
import com.michalrys.lcbc.femfile.XlsxFile;
import com.michalrys.lcbc.tools.License;
import com.michalrys.lcbc.tools.Statistics;
import com.michalrys.lcbc.tools.Version;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AppRun {
    public static final String VERSION = "1.0.1";
    public static final String DATE = "2019-11-25";
    public static final String AUTHOR = "Michał Ryś";

    public static List<MVC.View> views = new ArrayList<>();
    public static String status = "Drag and drop here BDF / FEM file.";
    public static String fileName;
    public static boolean isRunning;

    public static void main(String[] args) {
        Statistics.writeData();
        if(!License.isValid() || !Version.isValid(VERSION)) {
            return;
        }
        isRunning = false;

        if (args.length != 0) {
            String filePath = getFilePath(args);
            fileName = filePath;
            notifyAllViewsForNewFileName();
            doExtractionXmlsOrBdfFile(filePath);
        } else {
            System.out.println("No file was selected. Nothing was done.");
        }
    }

    private static String getFilePath(String[] args) {
        return convertBackSlashesToSlashes(args[0]);
    }

    private static String convertBackSlashesToSlashes(String filePath) {
        return filePath.replaceAll("\\\\", "/");
    }

    private static void doExtractionXmlsOrBdfFile(String filePath) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                isRunning = true;

                if(!License.isValid()) {
                    return;
                }

                updateStatus("LC/BC: initilizing...");
                if (filePath.contains(".bdf") || filePath.contains(".fem")) {
                    extractionToXmlsFile(filePath);
                    isRunning = false;
                } else if (filePath.contains(".xlsx")) {
                    extractionToBdfFile(filePath);
                    isRunning = false;
                } else {
                    wrongTypeOfFileMessage();
                    isRunning = false;
                }
            }
        });
        thread.start();
    }

    private static void extractionToXmlsFile(String filePath) {
        FemFile femFile = new AnyFemFile(filePath);
        femFile.readLoadcasesAndLoadCols();
        femFile.updateLoadCols();

        //TODO here is example of data extraction for excel table
//        List<Loadcol> listOfGrav = ((AnyFemFile) femFile).getListOfGrav();
//        for(Loadcol grav: listOfGrav) {
//            System.out.println(grav.getNastranTable().toString());
//            double g = Double.parseDouble(grav.getNastranTable().get(3));
//            System.out.println("grag g = " + (g + 1.0));
//        }

        ExcelFile excelFile = new ExcelFile(filePath);
        excelFile.setUpWorkbook();
        excelFile.fillCellsWith(femFile);
        excelFile.writeToFile();
    }

    private static void extractionToBdfFile(String filePath) {
        try {
            FemFile xlsxFile = new XlsxFile(filePath);
            xlsxFile.readLoadcasesAndLoadCols();
            xlsxFile.writeToFile();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Problem with reading xlsx file.");
        }


//        try {
//            Thread.sleep(1000);
//            AppRun.updateStatus("LC/BC: here will be conversion XLSX to BDF. Not available at the moment");
//            AppRun.updateStatus("Finished.");
//            AppRun.updateStatus("-------------------------------------");
//            System.out.println("HERE WILL BE CONVERSION TO BDF FILE");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    private static void wrongTypeOfFileMessage() {
        try {
            Thread.sleep(1000);
            AppRun.updateStatus("LC/BC: wrong type of file. Only BDF / FEM / XLSX files are allowable.");
            AppRun.updateStatus("Finished.");
            AppRun.updateStatus("-------------------------------------");
            System.out.println("HERE WILL BE CONVERSION TO BDF FILE");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void addView(MVC.View view) {
        if (!views.contains(view)) {
            views.add(view);
        }
    }

    public static void deleteView(MVC.View view) {
        if (views.contains(view)) {
            views.remove(view);
        }
    }

    // here is Obervable Pattern quick implementation
    public static void notifyAllViews() {
        for (MVC.View view : views) {
            view.update();
        }
    }

    public static void notifyAllViewsForNewFileName() {
        for (MVC.View view : views) {
            view.updateFileName();
        }
    }

    public static void updateStatus(String newLine) {
        status = status + "\n" + newLine;
    }

    public static void updateStatusWindowOnly() {
        notifyAllViews();
    }
}