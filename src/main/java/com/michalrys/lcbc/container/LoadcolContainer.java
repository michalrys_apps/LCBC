package com.michalrys.lcbc.container;

import com.michalrys.lcbc.loadcols.LoadType;
import com.michalrys.lcbc.loadcols.Loadcol;
import com.michalrys.lcbc.loadcols.LoadcolFactory;

import java.util.ArrayList;
import java.util.List;

public class LoadcolContainer implements Container {
    private final LoadcolFactory loadcolFactory;
    private final List<Loadcol> loadcolColorsOnly;
    private final List<Loadcol> loadcolNamesOnly;
    private final List<Loadcol> loadcolGravs;
    private final List<Loadcol> loadcolSpcs;
    private final List<Loadcol> loadcolSpcAdds;
    private final List<Loadcol> loadcolLoads;
    private final List<Loadcol> loadcolPload4s;
    private final List<Loadcol> loadcolForces;
    private final List<Loadcol> loadcolMoment;

    public LoadcolContainer() {
        loadcolFactory = new LoadcolFactory();
        loadcolColorsOnly = new ArrayList<>();
        loadcolNamesOnly = new ArrayList<>();
        loadcolGravs = new ArrayList<>();
        loadcolSpcs = new ArrayList<>();
        loadcolSpcAdds = new ArrayList<>();
        loadcolLoads = new ArrayList<>();
        loadcolPload4s = new ArrayList<>();
        loadcolForces = new ArrayList<>();
        loadcolMoment = new ArrayList<>();
    }

    @Override
    public Loadcol update(String readLine) {
        Loadcol loadcol = loadcolFactory.make(readLine);

        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolColorsOnly, LoadType.COLOR_ONLY);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolNamesOnly, LoadType.NAME_ONLY);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolGravs, LoadType.GRAV);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolSpcs, LoadType.SPC);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolSpcAdds, LoadType.SPCADD);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolLoads, LoadType.LOAD);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolPload4s, LoadType.PLOAD4);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolForces, LoadType.FORCE);
        updateIfPossibleLoadcolsDescribedById(loadcol, loadcolMoment, LoadType.MOMENT);

        return loadcol;
    }

    private void updateIfPossibleLoadcolsDescribedById(Loadcol givenLoadcol, List<Loadcol> loadcolsToCheck, LoadType loadType) {
        if (givenLoadcol.getLoadType().equals(loadType)) {
            if (loadcolsToCheck.contains(givenLoadcol)) {
                loadcolsToCheck.remove(givenLoadcol);
                loadcolsToCheck.add(givenLoadcol);
                return;
            }
            loadcolsToCheck.add(givenLoadcol);
        }
    }

    @Override
    public void updateByAdditionalData(Loadcol currentLoadcol, String readLine) {
        currentLoadcol.addContinuationData(readLine);
    }

    @Override
    public void updateDescriptionsAndColors() {
        updateDescription(loadcolSpcs);
        updateDescription(loadcolSpcAdds);
        updateDescription(loadcolLoads);
        updateDescription(loadcolPload4s);
        updateDescription(loadcolForces);
        updateDescription(loadcolMoment);
        updateDescription(loadcolGravs);

        updateColor(loadcolSpcs);
        updateColor(loadcolSpcAdds);
        updateColor(loadcolLoads);
        updateColor(loadcolPload4s);
        updateColor(loadcolForces);
        updateColor(loadcolMoment);
        updateColor(loadcolGravs);
    }

    private void updateDescription(List<Loadcol> loadcolContainerToUpdate) {
        for (Loadcol loadcolName : loadcolNamesOnly) {
            for (Loadcol loadcolToUpdate : loadcolContainerToUpdate) {
                if (loadcolToUpdate.getId().equals(loadcolName.getId())) {
                    loadcolToUpdate.setDescription(loadcolName.getDescription());
                }
            }
        }
    }

    private void updateColor(List<Loadcol> loadcolContainerToUpdate) {
        for (Loadcol loadcolColor : loadcolColorsOnly) {
            for (Loadcol loadcolToUpdate : loadcolContainerToUpdate) {
                if (loadcolToUpdate.getId().equals(loadcolColor.getId())) {
                    loadcolToUpdate.setColorId(loadcolColor.getColorId());
                }
            }
        }
    }

    @Override
    public void printAll() {
//        printContainer(loadcolNamesOnly, "Loadcol names");
//        printContainer(loadcolColorsOnly, "Loadcol colors");
//        printContainer(loadcolGravs, "Loadcol gravs");
//        printContainer(loadcolSpcs, "Loadcol spcs");
//        printContainer(loadcolLoads, "Loadcol loads");
//        printContainer(loadcolPload4s, "Loadcol pload4s");
//        printContainer(loadcolForces, "Loadcol forces");
//        printContainer(loadcolMoment, "Loadcol moments");
//        printContainer(loadcolSpcAdds, "Loadcol spcadds");
    }

    private void printContainer(List<Loadcol> loadcols, String givenName) {
        System.out.println("------------------");
        System.out.println(givenName + " only:");
        for (int i = 0; i < loadcols.size(); i++) {
            System.out.println(i + "): " + loadcols.get(i));
        }
    }

    //TODO test for extracting data
    @Override
    public List<Loadcol> getLoadcolGravs() {
        return loadcolGravs;
    }

    @Override
    public List<Loadcol> getLoadcolSpcs() {
        return loadcolSpcs;
    }

    @Override
    public List<Loadcol> getLoadcolSpcAdds() {
        return loadcolSpcAdds;
    }

    @Override
    public List<Loadcol> getLoadcolLoads() {
        return loadcolLoads;
    }

    @Override
    public List<Loadcol> getLoadcolPload4s() {
        return loadcolPload4s;
    }

    @Override
    public List<Loadcol> getLoadcolForces() {
        return loadcolForces;
    }

    @Override
    public List<Loadcol> getLoadcolMoment() {
        return loadcolMoment;
    }

    @Override
    public void add(Loadcol loadcol) {
        LoadType loadType = loadcol.getLoadType();
        switch (loadType) {
            case GRAV:
                loadcolGravs.add(loadcol);
                break;
            case FORCE:
                loadcolForces.add(loadcol);
                break;
            case MOMENT:
                loadcolMoment.add(loadcol);
                break;
            case PLOAD4:
                loadcolPload4s.add(loadcol);
                break;
            case SPC:
                loadcolSpcs.add(loadcol);
                break;
            case SPCADD:
                loadcolSpcAdds.add(loadcol);
                break;
            case LOAD:
                loadcolLoads.add(loadcol);
                break;
        }
    }
}