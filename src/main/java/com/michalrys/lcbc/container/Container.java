package com.michalrys.lcbc.container;

import com.michalrys.lcbc.loadcols.Loadcol;

import java.util.List;

public interface Container {
    Loadcol update(String readLine);

    void updateByAdditionalData(Loadcol currentLoadcol, String readLine);

    void printAll();

    void updateDescriptionsAndColors();

    void add(Loadcol loadcol);

    //TODO test for extracting data
    List<Loadcol> getLoadcolGravs();

    List<Loadcol> getLoadcolSpcs();

    List<Loadcol> getLoadcolSpcAdds();

    List<Loadcol> getLoadcolLoads();

    List<Loadcol> getLoadcolPload4s();

    List<Loadcol> getLoadcolForces();

    List<Loadcol> getLoadcolMoment();
}
