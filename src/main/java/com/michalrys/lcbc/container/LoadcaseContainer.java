package com.michalrys.lcbc.container;

import com.michalrys.lcbc.loadcase.Loadcase;

import java.util.ArrayList;
import java.util.List;

public class LoadcaseContainer {
    private final List<Loadcase> loadcases;

    public LoadcaseContainer() {
        loadcases = new ArrayList<>();
    }

    public Loadcase update(String readLine) {
        Loadcase loadcase = new Loadcase(readLine);

        if (loadcases.contains(loadcase)) {
            int lcId = loadcases.indexOf(loadcase);
            Loadcase loadcaseInContainer = loadcases.get(lcId);
            loadcaseInContainer.updateNullOnlyBy(loadcase);
            return loadcaseInContainer;
        }
        loadcases.add(loadcase);
        return loadcase;
    }

    public void updateByAdditionalData(Loadcase currentLoadcase, String readLine) {
        currentLoadcase.addContinuationData(readLine);
    }

    public void printAll() {
        printContainer(loadcases);
    }

    private void printContainer(List<Loadcase> loadcases) {
//        System.out.println("------------------");
//        System.out.println("All loadcases :");
//        for (int i = 0; i < loadcases.size(); i++) {
//            System.out.println("-- " + i + ") ------------------");
//            System.out.print(loadcases.get(i).print());
//        }
//        System.out.println("===== LOADCASES: =======");
//        System.out.println(loadcases.toString());
    }

    public List<Loadcase> getList() {
        return loadcases;
    }

    public void add(Loadcase loadcase) {
        loadcases.add(loadcase);
    }
}