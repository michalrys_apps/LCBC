package com.michalrys.controller;

import com.michalrys.MVC;
import com.michalrys.lcbc.AppRun;

public class LCBCappRunController implements MVC.Controller {
    private final MVC.View view;

    public LCBCappRunController(MVC.View view) {
        this.view = view;
    }

    @Override
    public void runConversionsWithInputsArgs(String[] args) {
        if (args.length != 0) {
            AppRun.main(args);
            updateStatusThread();
        }
    }

    private void updateStatusThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                while (AppRun.isRunning) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    AppRun.updateStatusWindowOnly();
                    view.statusGoEnd();
                }
            }
        });
        thread.start();
    }
}
