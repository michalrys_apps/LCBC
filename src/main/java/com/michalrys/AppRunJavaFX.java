package com.michalrys;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Random;

public class AppRunJavaFX extends Application {
    public static Stage mainWindowStage;
    public static String[] inputArgs;

    @Override
    public void start(Stage primaryStage) throws Exception {
        mainWindowStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("view/MainWindow.fxml"));
        primaryStage.setScene(new Scene(root));

        primaryStage.setTitle("LC/BC");

        //primaryStage.getIcons().add(new Image("file:./src/resources/icon.png"));
        primaryStage.setResizable(false);
        primaryStage.setAlwaysOnTop(true);
        primaryStage.setX(50 + getRandomCoordinate());
        primaryStage.setY(50 + getRandomCoordinate());
        primaryStage.initStyle(StageStyle.DECORATED);
        primaryStage.show();
    }

    public static void main(String[] args) {
        inputArgs = args;
        launch(args);
    }

    private int getRandomCoordinate() {
        Random random = new Random();
        return 5 * random.nextInt(10);
    }
}
