package com.michalrys;

public interface Observable {
    void addView(MVC.View view);

    void deleteView(MVC.View view);

    void notifyAllViews();
}
