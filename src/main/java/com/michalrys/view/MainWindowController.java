package com.michalrys.view;

import com.michalrys.AppRunJavaFX;
import com.michalrys.MVC;
import com.michalrys.controller.LCBCappRunController;
import com.michalrys.lcbc.AppRun;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable, MVC.View {
    private LCBCappRunController controller;

    @FXML
    public Label wFile;
    @FXML
    public TextArea wStatus;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controller = new LCBCappRunController(this);
        AppRun.addView(this);
        runWithInputArgs();
    }

    public void statusDragOver(DragEvent dragEvent) {
        dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        dragEvent.consume();
    }

    public void statusDragDropped(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        List<File> files = dragboard.getFiles();
        dragEvent.setDropCompleted(true);
        dragEvent.consume();

        File lastFile = files.get(files.size() - 1);
        String[] inputArg = new String[1];
        inputArg[0] = lastFile.getAbsolutePath();

        controller.runConversionsWithInputsArgs(inputArg);
    }

    @Override
    public void runWithInputArgs() {
        controller.runConversionsWithInputsArgs(AppRunJavaFX.inputArgs);
    }

    @Override
    public void update() {
        updateWindowStatus();
    }

    @Override
    public void updateFileName() {
        String fileName = AppRun.fileName;
        int fileLength = 80;
        if (fileName.length() >= fileLength) {
            fileName = "..." + fileName.substring(fileName.length() - fileLength);
        }
        wFile.setText("File: " + fileName);
    }

    private void updateWindowStatus() {
        wStatus.setText(AppRun.status);
        wStatus.setScrollTop(Double.MAX_VALUE);
    }

    @Override
    public void statusGoEnd(){
        wStatus.selectEnd();
        wStatus.deselect();
    }
}
