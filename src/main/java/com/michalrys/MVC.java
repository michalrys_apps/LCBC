package com.michalrys;

public interface MVC {
    interface Controller {
        void runConversionsWithInputsArgs(String[] args);
    }

    interface View {
        void update();

        void updateFileName();

        void runWithInputArgs();

        void statusGoEnd();
    }
}
