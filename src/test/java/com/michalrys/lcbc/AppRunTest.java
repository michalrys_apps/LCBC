package com.michalrys.lcbc;

import com.michalrys.AppRunJavaFX;
import org.junit.Test;

public class AppRunTest {
    @Test
    public void shouldAnswerWithTrue() {
        // given
        AppRun appRun = new AppRun();

        String filePath1 = "./src/main/resources/Fem_test_LC2_loads_only.bdf";
//        String filePath2 = "./src/main/resources/Fem_test_LC.bdf";
        String filePath2bdf = "./src/main/resources/Fem_test_LC.bdf";
        String filePath2 = "./src/main/resources/Fem_test_LC.xlsx";
        String filePath3 = "./src/main/resources/Fem_test_Obciazenia_AP.bdf";
        String filePath4 = "./src/main/resources/more_lc_2.bdf";
//        String filePath4 = "./src/main/resources/more_lc_2.xlsx";
        String filePath5bdf = "./src/main/resources/TEST01_PUDLO.bdf";
        String [] filePaths = new String[4];
        filePaths[0] = filePath4;
//        filePaths[0] = filePath2;
        filePaths[1] = filePath1;
        filePaths[2] = filePath3;
        filePaths[3] = filePath4;
        //appRun.main(filePaths);

        //AppRunJavaFX.main(filePaths);

        String [] argsToRunSingleFile = new String[1];
//        argsToRunSingleFile[0] = filePath2bdf;
        argsToRunSingleFile[0] = filePath5bdf;
//        argsToRunSingleFile[0] = filePath2;
        AppRunJavaFX.main(argsToRunSingleFile);

        // when

        // then
    }
}
